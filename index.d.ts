declare namespace SankeyGraph {
  export class Position {
    x: number;
    y: number;

    public clone(): Position;

    constructor(x: number, y: number);
  }

  export class Size {
    width: number;
    height: number;
  }

  export interface Drawable {
    draw(context: Context, cursor: Position): void;
  }

  export class Context {
    drawArea: HTMLElement;
    infoCardContainer: HTMLElement;
    canvasContainer: HTMLElement;

    constructor(drawArea: HTMLElement, size: Size);

    public clear(): void;
  }

  export class GraphConfiguration {
    drawArea: HTMLElement;
    columnCount: number;
    className?: string;
    sidePadding?: number;
    marginBetweenDrawables?: number;
    lineColor?: string;

    constructor(drawArea: HTMLElement,
      columnCount: number,
      options?: {
        className?: string,
        sidePadding?: number,
        marginBetweenDrawables?: number,
        lineColor?: string
      }
    );
  }

  export class PackConfiguration {
    sidePadding: number;
    columnCount: number;
    columnWidth: number;
    drawingMode: string;
    rowMargin: number;
    lineColor: string;
    connectionPointPosition: number;

    constructor(drawingMode: string, graphConfig: GraphConfiguration, options?: {
      rowMargin?: number;
      connectionPointPosition?: number;
    });
  }

  export class InfocardConfig {
    colspan: number;
    fillwidth: boolean;
    connectionPointColors?: {
      prev: string,
      next: string
    };
    defaultClassName?: string;

    constructor(options?: {
      connectionPointColors?: any,
      defaultClassName?: string
    });
  }

  export class InfoCard {
    identifier: string;
    content: HTMLElement;
    associatedPack: Pack;
    connections: {
      prev: Connection[];
      next: Connection[];
    }
    drawn: boolean;
    config: InfocardConfig;

    constructor(identifier: string, content: HTMLElement, config: InfocardConfig);

    public attachOnMouseHoverEventListener(): void;
  }

  export class Pack implements Drawable {
    constructor(grid: InfoCard[][], connections: Map<string, number[][]>, config?: PackConfiguration);

    public setConfig(config: PackConfiguration): void;

    public draw(context: Context, cursor: Position): void;

    public toggleDim(state: boolean): void;
  }

  export class Graph {
    private packs: Pack[];
    private configuration: GraphConfiguration;
    private infocardContainer: HTMLElement;
    private canvasElement: HTMLElement;
    private columnCount: number;
    private columnWidth: number;
    private context: Context;

    constructor(config: GraphConfiguration, packs: Pack[]);

    public setData(packs: Pack[]): void;

    public draw(): void;

    public redraw(): void;

    public redrawWithData(packs: Pack[]): void;
  }

  export class ConnectionPoint {
    position: Position;
    element: HTMLElement;
  }

  export class Connection {
    path: SVGPathElement;
    thing: InfoCard;
  }
}