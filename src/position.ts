export class Position {
  x: number;
  y: number;

  public clone(): Position {
    return new Position(this.x, this.y);
  }

  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }
}