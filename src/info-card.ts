import { Connection } from './connection';
import { InfocardConfig } from './info-card-config';
import { Pack } from './pack';

export class InfoCard {
  identifier: string;
  content: HTMLElement;
  associatedPack: Pack;
  connections: {
    prev: Array<Connection>;
    next: Array<Connection>;
  }
  drawn: boolean;
  config: InfocardConfig;

  constructor(identifier: string, content: HTMLElement, config: InfocardConfig) {
    this.identifier = identifier;
    this.content = content;
    this.drawn = false;
    this.connections = { prev: [], next: [] };
    this.config = config;
  }

  public attachOnMouseHoverEventListener(): void {
    this.content.addEventListener("mouseover", (ev: MouseEvent) => {
      this.associatedPack.toggleDim(true);
      this.highlightConnectedCards();
    }, false);
    this.content.addEventListener("mouseleave", (ev: MouseEvent) => {
      this.associatedPack.toggleDim(false);
    }, false);
  }

  private highlightConnectedCards(): void {
    this.content.style.opacity = "1";
    this.highlighCardsNext(this.connections.next);
    this.highlightCardsPrev(this.connections.prev);
  }

  private highlighCardsNext(conns: Array<Connection>): void {
    conns.forEach((conn: Connection, index: number) => {
      conn.path.setAttributeNS(null, "style", "stroke-opacity: 1 !important");
      conn.thing.content.style.opacity = "1";
      this.highlighCardsNext(conn.thing.connections.next);
    });
  }

  private highlightCardsPrev(conns: Array<Connection>): void {
    conns.forEach((conn: Connection, index: number) => {
      conn.path.setAttributeNS(null, "style", "stroke-opacity: 1 !important");
      conn.thing.content.style.opacity = "1";
      this.highlightCardsPrev(conn.thing.connections.prev);
    });
  }
}