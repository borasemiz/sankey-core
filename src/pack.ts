import { InfoCard } from "./info-card";
import { PackConfiguration } from "./pack-configuration";
import { Drawable } from "./drawable";
import { Context } from "./context";
import { Position } from './position';

export class Pack implements Drawable {
  private grid: InfoCard[][];
  private connections: Map<string, number[][]>;
  private config: PackConfiguration;
  private infocardWidth: number;
  private context: Context;

  constructor(grid: InfoCard[][], connections: Map<string, number[][]>, config?: PackConfiguration) {
    this.grid = grid;
    this.connections = connections;
    this.config = config;
    this.infocardWidth = typeof config === 'undefined'
      ? null : config.columnWidth - (2 * config.sidePadding);
  }

  public setConfig(config: PackConfiguration): void {
    this.config = config;
    this.infocardWidth = config.columnWidth - (2 * config.sidePadding);
  }

  public draw(context: Context, cursor: Position): void {
    this.putInfocards(context, cursor, this.config.drawingMode);
    this.connectInfocards(context);
    this.context = context;
  }

  public toggleDim(state: boolean): void {
    let opacity: number = state ? 0.5 : 1.0;
    let svgElement: SVGElement = this.context.canvasContainer.querySelector("#canvas");
    for (let i = 0; i < svgElement.children.length; i++) {
      svgElement.children[i].setAttributeNS(null, "style", "stroke-opacity: " + opacity + "" + " !important");
    }
    for (let i = 0; i < this.context.infoCardContainer.children.length; i++) {
      (<HTMLElement>this.context.infoCardContainer.childNodes[i]).style.opacity = opacity + "";
    }
  }

  private connectInfocards(context: Context): void {
    this.grid.forEach((columns: Array<InfoCard>, rowIndex: number) => {
      columns.forEach((cell: InfoCard, columnIndex: number) => {
        if (cell === null) return;
        let connection: Array<Array<number>> = this.connections.get(cell.identifier);
        if (connection === null || connection === undefined) return;
        connection.forEach((conn: Array<number>, index: number) => {
          this.connectTwoInfoCards(cell, this.grid[conn[0]][conn[1]], context);
        });
      });
    });
  }

  private putInfocards(context: Context, cursor: Position, mode: string): void {
    switch (mode) {
      case "compact": this.putInfocardsInCompact(context, cursor); break;
      case "loose":
      default:
        this.putInfocardsInLoose(context, cursor); break;
    }

  }

  private putInfocardsInCompact(context: Context, cursor: Position): void {
    let maxHeight: number = 0, tempCursorY: number = cursor.y;
    for (let columnIndex = 0; columnIndex < this.config.columnCount; columnIndex++) {
      cursor.x = this.config.sidePadding + (this.config.columnWidth * columnIndex);
      cursor.y = tempCursorY;
      let totalHeight: number = 0;
      this.grid.forEach((columns: Array<InfoCard>) => {
        if (columns[columnIndex] === null) return;
        let height: number = this.drawInfocard(cursor, context, columns[columnIndex]);
        columns[columnIndex].associatedPack = this;
        columns[columnIndex].attachOnMouseHoverEventListener();
        cursor.y += height + this.config.rowMargin;
        totalHeight += height + this.config.rowMargin;
      });
      maxHeight = totalHeight > maxHeight ? totalHeight : maxHeight;
    }
    cursor.y = maxHeight;
  }

  private putInfocardsInLoose(context: Context, cursor: Position): void {
    this.grid.forEach((columns: InfoCard[], rowIndex: number) => {
      let maxHeight: number = 0;
      cursor.x = this.config.sidePadding;
      columns.forEach((card: InfoCard, columnIndex: number) => {
        if (card === null) {
          cursor.x += this.config.columnWidth;
          return;
        }
        let height: number = this.drawInfocard(cursor, context, card);
        cursor.x += this.config.columnWidth;
        maxHeight = maxHeight < height ? height : maxHeight;
        card.associatedPack = this;
        card.attachOnMouseHoverEventListener();
      });
      cursor.y += maxHeight + this.config.rowMargin;
    })
  }

  private drawInfocard(cursor: Position, context: Context, card: InfoCard): number {
    const styleString: string = `
      position: absolute;
      width: ${this.infocardWidth}px;
      top: ${cursor.y}px;
      left: ${cursor.x}px;
      box-sizing: border-box;
      transition: opacity 0.2s linear;
    `;
    card.content.setAttribute('style', styleString);
    card.content.className = card.config.defaultClassName + " " + card.content.className;
    context.infoCardContainer.appendChild(card.content);
    card.drawn = true;
    return card.content.offsetHeight;
  }

  private connectTwoInfoCards(start: InfoCard, end: InfoCard, context: Context): void {
    let startY = this.config.connectionPointPosition === null ?
      Math.floor(start.content.offsetHeight / 2) : this.config.connectionPointPosition;
    let sPosition: Position = new Position(
      parseInt(start.content.style.left.replace("px", "")) + start.content.offsetWidth,
      parseInt(start.content.style.top.replace("px", "")) + startY
    );

    let endY = this.config.connectionPointPosition === null ?
      Math.floor(end.content.offsetHeight / 2) : this.config.connectionPointPosition;
    let ePosition: Position = new Position(
      parseInt(end.content.style.left.replace("px", "")),
      parseInt(end.content.style.top.replace("px", "")) + endY
    );

    let svg: SVGElement = context.canvasContainer.querySelector("#canvas");
    let path: SVGPathElement = this.drawCurvedLine(sPosition, ePosition, svg);
    this.putConnectionPoint(start, "start");
    this.putConnectionPoint(end, "end");
    start.connections.next.push({ path: path, thing: end });
    end.connections.prev.push({ path: path, thing: start });
  }

  private drawCurvedLine(start: Position, end: Position, svg: SVGElement, debugMode: boolean = false): SVGPathElement {
    let pathElement: SVGPathElement = document.createElementNS("http://www.w3.org/2000/svg", "path");
    let xCenter: number = start.x + Math.floor((end.x - start.x) / 2);

    let pathString = "M " + start.x + "," + start.y + " C " + xCenter + "," + start.y + " " + xCenter + "," + end.y +
      " " + end.x + "," + end.y;
    pathElement.setAttributeNS(null, "d", pathString);
    pathElement.setAttributeNS(null, "fill", "transparent");
    pathElement.setAttributeNS(null, "stroke", this.config.lineColor);
    pathElement.setAttributeNS(null, "stroke-width", "2");
    pathElement.style.transition = "stroke-opacity 0.2s linear";
    svg.appendChild(pathElement);

    return pathElement;
  }

  private putConnectionPoint(infocard: InfoCard, where: string): void {
    let point: HTMLDivElement = document.createElement("div");
    point.className = "connection-point";
    point.style.boxSizing = "border-box";
    point.style.width = "12px";
    point.style.height = "12px";
    point.style.position = "absolute";
    point.style.borderRadius = "50%";

    let yPos: number = this.config.connectionPointPosition === null ?
      Math.floor(infocard.content.offsetHeight / 2) : this.config.connectionPointPosition;
    switch (where) {
      case "end":
        point.style.left = "-6px";
        point.style.border = "1px solid " + infocard.config.connectionPointColors.next;
        break;
      default:
        point.style.right = "-6px";
        point.style.border = "1px solid " + infocard.config.connectionPointColors.prev;
        break;
    }
    point.style.top = (yPos - 6) + "px";
    infocard.content.appendChild(point);
  }
}