import { Size } from "./size";

export class Context {
  drawArea: HTMLElement;
  infoCardContainer: HTMLElement;
  canvasContainer: HTMLElement;

  constructor(drawArea: HTMLElement, size: Size) {
    this.drawArea = drawArea;
    let scrollableContent = this.prepareScrollableContent(size);
    this.canvasContainer = this.prepareCanvasElement(size);
    this.infoCardContainer = this.prepareInfocardContainer(size);

    scrollableContent.appendChild(this.canvasContainer);
    scrollableContent.appendChild(this.infoCardContainer);
    this.drawArea.appendChild(scrollableContent);
  }

  public clear(): void {
    this.infoCardContainer.innerHTML = "";
    this.canvasContainer.querySelector("#canvas").innerHTML = "";
  }

  private prepareScrollableContent(size: Size): HTMLElement {
    let content: HTMLElement = document.createElement("div");
    content.style.overflowY = "auto";
    content.style.overflowX = "hidden";
    content.style.width = size.width + "px";
    content.style.height = size.height + "px";
    content.style.position = "absolute";
    return content;
  }

  private prepareCanvasElement(size: Size): HTMLElement {
    let element: HTMLElement = document.createElement("div");
    element.style.width = size.width + "px";
    element.style.position = "absolute";
    element.style.backgroundColor = "transparent";

    let svg: SVGElement = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    svg.setAttribute("id", "canvas");
    svg.setAttribute("width", size.width + "");
    svg.setAttribute("style", "overflow: hidden; position: relative; left: -0.25px;");
    svg.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
    element.appendChild(svg);

    return element;
  }

  private prepareInfocardContainer(size: Size): HTMLElement {
    let container: HTMLElement = document.createElement("div");
    container.style.width = size.width + "px";
    container.style.height = size.height + "px";
    container.style.position = "absolute";
    return container;
  }
}