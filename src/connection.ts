import { InfoCard } from './info-card';

export class Connection {
  path: SVGPathElement;
  thing: InfoCard;
}