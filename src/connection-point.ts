import { Position } from './position';

export class ConnectionPoint {
  position: Position;
  element: HTMLElement;
}