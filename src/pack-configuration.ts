import { GraphConfiguration } from "./graph-configuration";

export class PackConfiguration {
  sidePadding: number;
  columnCount: number;
  columnWidth: number;
  drawingMode: string;
  rowMargin: number;
  lineColor: string;
  connectionPointPosition: number;

  constructor(drawingMode: string, graphConfig: GraphConfiguration, options?: {
    rowMargin?: number;
    connectionPointPosition?: number;
  }) {
    this.drawingMode = drawingMode;
    this.columnCount = graphConfig.columnCount;
    this.columnWidth = Math.floor(graphConfig.drawArea.offsetWidth / graphConfig.columnCount);
    this.sidePadding = (this.columnWidth * graphConfig.sidePadding / 100) / 2;
    this.lineColor = graphConfig.lineColor;
    if (typeof options !== 'undefined') {
      this.rowMargin = typeof options.rowMargin === 'undefined' ? 30 : options.rowMargin;
      this.connectionPointPosition = typeof options.connectionPointPosition === 'undefined' ? null : options.connectionPointPosition;
    }
  }
}