export class InfocardConfig {
  colspan: number;
  fillwidth: boolean;
  connectionPointColors?: {
    prev: string,
    next: string
  } = { prev: 'white', next: 'white' }
  defaultClassName?: string = 'info-card';

  constructor(options?: {
    connectionPointColors?: any,
    defaultClassName?: string
  }) {
    this.colspan = 1;
    this.fillwidth = false;
    if (typeof options !== 'undefined') {
      Object.keys(<any>options).forEach(key => {
        (<any>this)[key] = (<any>options)[key];
      });
    }
  }
}