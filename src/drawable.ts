import { Context } from './context';
import { Position } from './position';

export interface Drawable {
  draw(context: Context, cursor: Position): void;
}