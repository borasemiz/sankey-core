import { Size } from "./size";

export class GraphConfiguration {
  drawArea: HTMLElement;
  columnCount: number;
  className?: string = 'info-card';
  sidePadding?: number = 60;
  marginBetweenDrawables?: number = 50;
  lineColor?: string = 'white';

  constructor(drawArea: HTMLElement,
    columnCount: number,
    options?: {
      className?: string,
      sidePadding?: number,
      marginBetweenDrawables?: number,
      lineColor?: string
    }
  ) {

    this.drawArea = drawArea;
    this.columnCount = columnCount;
    if (typeof options !== 'undefined') {
      Object.keys(<any>options).forEach(opts => {
        (<any>this)[opts] = (<any>options)[opts];
      });
    }
  }
}