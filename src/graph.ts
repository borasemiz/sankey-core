import { Drawable } from "./drawable";
import { GraphConfiguration } from "./graph-configuration";
import { Context } from "./context";
import { Position } from './position';
import { Pack } from "./pack";

export class Graph {
  private packs: Pack[];
  private configuration: GraphConfiguration;
  private infocardContainer: HTMLElement;
  private canvasElement: HTMLElement;
  private columnCount: number;
  private columnWidth: number;
  private context: Context;

  constructor(config: GraphConfiguration, packs: Pack[]) {
    this.configuration = config;
    this.columnCount = this.configuration.columnCount;
    this.packs = packs;
    this.context = new Context(this.configuration.drawArea, {
      width: this.configuration.drawArea.offsetWidth,
      height: this.configuration.drawArea.offsetHeight
    });
  }

  public setData(things: Array<Pack>): void {
    this.packs = things;
    this.packs.forEach(pack => {
    });
  }

  public draw(): void {
    let cursor: Position = new Position(0, this.configuration.marginBetweenDrawables);
    this.packs.forEach((pack: Pack, index: number) => {
      pack.draw(this.context, cursor);
      cursor.x = 0;
      cursor.y += this.configuration.marginBetweenDrawables;
    });
    this.resizeHeightOfContext(cursor.y);
  }

  public redraw(): void {
    this.clear();
    this.draw();
  }

  public redrawWithData(packs: Pack[]): void {
    this.setData(packs);
    this.redraw();
  }

  private clear(): void {
    this.context.clear();
  }

  private resizeHeightOfContext(height: number): void {
    this.context.infoCardContainer.style.height = height + "px";
    this.context.canvasContainer.querySelector("#canvas").setAttribute("height", height + "px");
  }
}