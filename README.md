# Sankey Diagram Core

This repository hosts the sankey diagram's core. However, the sankey diagram here is a little bit different than an official one.

## Quickstart

In order to build and run the demo, you need to have node and npm installed in your machine. Open up a terminal and enter the root directory of this project. Issue the following commands in order:

```
npm install
npm run start:dev
```

Your after you entered the last command, a development server is started from `localhost:9090`. Go there and you will see the demo.

### For a Production Build

```
npm install
npm run build:prod
```

### But My Project Isn't Written in Typescript

You can copy and paste either the `dist/bundle.js` or the `dist/bundle.min.js` into your project directory and you are all set.

## User Guide

This is project is drawing sankey diagrams. Sankey diagram is a type of diagram in which nodes connect to each other in a sequential order from left to right. Sankey diagram that is natively provided by this project is a litle bit different from a traditional sankey diagrams. In a traditional sankey diagram, the connection lines' thickness are same as the nodes'. In this one however, it looks like as if boxes connects to each other with 1 pixel thick lines.

In order to visualize a picture about what I am talking about, you can head to the quickstart section, build the code and see the demo. In order to easily manage the code and make it easy to get back to, the project is written in [TypeScript](https://www.typescriptlang.org/). In addition that most of the projects that I am working are written in [Angular](https://angular.io/) framework, the project is decided to be written in TypeScript.

### Using the Library

#### Initializing the Graph

In order to draw with this library, you need to instantiate a `SankeyGraph.Graph` object. In order to initialize this Graph object, you need to provide a `SankeyGraph.GraphConfiguration` object and an array of `SankeyGraph.Drawable`s to indicate what to draw. You can give `null` for the second argument if you want to decide what to draw later. The `Drawable` is just an interface that has one method: `draw`. We will cover the details of this interface later on.

The `SankeyGraph.GraphConfiguration` object, as the name implies, used for configuring the Graph instance. With this object, you can configure a Graph's:

+ location to draw by providing an object of `HTMLElement` as the first argument,
+ column count by providing an integer as the second element,
+ infocards' main CSS class name, infocards' side pading, connection lines' color and margin between each drawable object.

The properties in the last bullet are all optional. You can see the API reference below for full detail.

To combine what we explain so far, in order to initialize a sankey graph in an HTML element with an id of `sankey-grap` we will do the following:

```html
<style>
    #sankey-graph {
        width: 1200px;
        height: 750px;
    }
</style>
<div id="sankey-graph"></div>
```

```javascript
var sankeyGraphConfig = new SankeyGraph.GraphConfiguration(document.getElementById("sankey-graph"), 3, {lineColor: "#01f1ff"});
var sankeyGraph = new SankeyGraph.Graph(sankeyGraphConfig, null);
```

This configuration specifes that we would like to draw a sankey graph that has 3 columns in total, that we want to draw it into the `#sankey-grap` and line colors `#01f1ff`.

#### Providing the Things to Draw

In summary, you need to provide and array of `SankeyGraph.Drawable`s to the `SankeyGraph.Graph` object by using the `setData` method.

Like we have said earlier, a `Drawable` is an interface that has a single method: `draw(context: SankeyGraph.Context, cursor: SankeyGraph.Position): void`. A direct implementation of this interface is `SankeyGraph.Pack`. This object is the actual sankey group.

To sum up, in order to draw, you need to provide an array of `SankeyGraph.Pack` objects. Pack is a group of sankey diagrams. You can draw multiple packs inside a graph.

In order to setup a Pack, you give it what I call a grid and connections which describes the connections between boxes. From this time, the _infocard_ will refer to a diagram node. The grid is a very simple data structure. It is just an array of arrays. The length of each array in the array has to be equal to graph's column count that you have set when you configured the `SankeyGraph.Graph` instance.

In order to draw the sankey nodes, we have created a class called `SankeyGraph.InfoCard`. You will instatiate objects from this class as nodes for the sankey graph. There are three very important attributes of `SankeyGraph.InfoCard`:

+ __identifier__: This is a very important attribute which has to be unique for each node. This identifier attribute will be used to find connections of this particular node and the nodes that connected to this. This attribute's type is `string`.
+ __content__: The most important attribute which describes the node's HTML content. You will provide an `HTMLElement` object to this attribute. You can sculpt this element anyway you like. However, there will be some style attributes that will be automaticly attached to this `HTMLElement` by the drawing function. These attributes are: `position`, `width`, `top`, `left`, `box-sizing`, `opacity`, `transition`. These attributes are attached to node's styling to ensure drawing to look correct. __IT IS IMPORTANT THAT YOU SHOULD NEVER EVER EVER OVERRIDE THESE ATTRIBUTES__. We will later on discuss how to correctly (without breaking anything) customize individual nodes.
+ __config__: You can provide an empty object for this attribute. In this version on the library, the attribute's task is to descrive what color the connection points will have. For example: `{connectionPointColors: '#01f1ff'}`.

Here is an example how to initialize a `SankeyGraph.InfoCard`:

```javascript
/** 
 * Create an HTML element through the DOM API.
 * I repeat. DO NOT OVERRIDE THE STYLE ATTRIBUTES MENTIONED ABOVE.
 */
var nodeOne = document.createElement("div");
nodeOne.innerHTML = "Sample text for  Node 1";
nodeOne.style.backgroundColor = "crimson";
nodeOne.style.border = "1px solid white";

/**
 * Construct a new InfoCard with a unique identifier, the HTML element created above
 * and an options.
 */
var infoCardOne = new SankeyGraph.InfoCard(
    "node-one-unique-identifier", nodeOne, 
    new SankeyGraph.InfocardConfig({connectionPointColors: 'green'})
);
```

After we initialized a node to draw, we can initialize the data of the `SankeyGraph.Pack` object. Earlier in this section, we talked about a structure something called _grid_. Let's construct it right away:

```javascript
/**
 * Initialize the grid. We have specified that our graph will contain three columns,
 * so the arrays is the grid array have to contain three elements. If you want leave one cell empty, put there
 * null.
 */
var grid = [
    [infoCardOne, null, null]
]
```

#### Providing the Connections

In order to describe which nodes will connect to which nodes, we will provide a connection descriptor object. This object is a simple ES6 Map object. ES6 maps are supported in all of the modern browsers including Internet Explorer 11 (only basic operations) and above. For mode information, please see this [MDN page](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map#Browser_compatibility).

The map object's keys will be the infocard's identifier that we specified. That entry in the map will describe the infocard's connections, e.g. which infocards will it connect to. The values for those keys will be an array of positions pointing to grid. Let me explain:

```javascript
/**
 * Create an another node to connect with first one.
 */
var nodeTwo = document.createElement("div");
nodeTwo.innerHTML = "Sample text for  Node 2";
nodeTwo.style.backgroundColor = "green";
nodeTwo.style.border = "1px solid white";

var infoCardTwo = new SankeyGraph.InfoCard(
    "node-two-unique-identifier", nodeTwo, 
    new SankeyGraph.InfocardConfig({connectionPointColors: 'green'})
);

/**
 * Put this node far right in the first row in the grid.
 */
grid[0][2] = infoCardTwo;

var connections = new Map();

/**
 * Set the infoCardOne's connection. Since this infocard will connect to the infoCardTwo,
 * and infoCardTwo is at the far right side of the first row in the grid, we will set its value
 * as the infoCardTwo's coordinate in grid. We will provide it inside an array. This way, we have the ability
 * to connect infoCardOne to multiple infocards.
 * 
 * Since infoCardTwo will already be connected to infoCardOne, we don't need to speciffy its connection explicitly.
 */
connections.set(infoCardOne.identifier, [ [0, 2] ]);
```

#### Instantiating a Pack Object

We have initialized a grid object for our Pack. In this chapter, we will attach that grid and connections to the Pack. But first we need to create the pack object:

```javascript
/**
 * Pack constructor takes three arguments. The grid object for the nodes, connections object for the connections of nodes and a
 * PackConfiguration object.
 * 
 * Pack configuration object, as the name implies, is used to configure the Pack that is being instantiated. It takes two values for its constructor:
 * the configuration object of the SankeyGraph.Graph and an options object.
 */
var pack = new SankeyGraph.Pack(grid, connections, new SankeyGraph.PackConfiguration(sankeyGraphConfig, {connectionPointPosition: 50}));
```

#### Putting It All Together

```html
<style>
    #sankey-graph {
        width: 1200px;
        height: 750px;
    }
</style>
<div id="sankey-graph"></div>
```

```javascript
/**
 * Initialize SankeyGraph.Graph.
 */
var sankeyGraphConfig = new SankeyGraph.GraphConfiguration(document.getElementById("sankey-graph"), 3, {lineColor: "#01f1ff"});
var sankeyGraph = new SankeyGraph.Graph(sankeyGraphConfig, null);

/**
 * Initialize a few nodes to draw.
 */
var nodeOne = document.createElement("div");
nodeOne.innerHTML = "Sample text for  Node 1";
nodeOne.style.backgroundColor = "crimson";
nodeOne.style.border = "1px solid white";

var nodeTwo = document.createElement("div");
nodeTwo.innerHTML = "Sample text for  Node 2";
nodeTwo.style.backgroundColor = "green";
nodeTwo.style.border = "1px solid white";

var infoCardOne = new SankeyGraph.InfoCard(
    "node-one-unique-identifier", nodeOne, 
    new SankeyGraph.InfocardConfig({connectionPointColors: 'crimson'})
);
var infoCardTwo = new SankeyGraph.InfoCard(
    "node-two-unique-identifier", nodeTwo, 
    new SankeyGraph.InfocardConfig({connectionPointColors: 'green'})
);

/**
 * Initialize the grid 
 */
var grid = [
    [infoCardOne, null, infoCardTwo]
]

/**
 * Initialize the connections
 */
var connections = new Map();
connections.set(infoCardOne.identifier, [ [0, 2] ]);

/**
 * Initialize a Pack
 */
var pack = new SankeyGraph.Pack(grid, connections, new SankeyGraph.PackConfiguration(sankeyGraphConfig, {connectionPointPosition: 50}));

/**
 * Configure the graph's data. We said that we need to set an array of Drawables to the graph's setData method.
 */
sankeyGraph.setData( [pack] );

/**
 * Grand finale! Draw it.
 */
sankeyGraph.draw();
```

## API Reference

All the classes below are contained in the `SankeyGraph` namespace.

### Graph _class_

The container class for all sankey graph.

```typescript
SankeyGraph.Graph(config: SankeyGraph.GraphConfiguration, things: Array<Drawable>)
```

+ __config__: A GraphConfiguration object that configures the graph.
+ __things__: Drawable array that is being drawn in this Graph.

#### Methods

+ __setData(things: Array&lt;Drawable&gt;)__: Method to use setting the data of this graph.
+ __draw()__: Method to draw the graph.
+ __redraw()__: Clears the graph and redraws everything.
+ __redrawWithData(things: Array&lt;Drawable&gt;)__: Sets a new data for the graph, represented by `things` and redraws the graph accordingly.

### GraphConfiguration _class_

An object to configure Graph.

```typescript
SankeyGraph.GraphConfiguration(drawArea: HTMLElement, columnCount: number, options?:{
    className?: string, 
    sidePadding?: number, 
    marginBetweenDrawables?: number, 
    lineColor?: string
})
```

+ __drawArea__: A HTMLElement object in which the Graph will be drawn into.
+ __columnCount__: Sets the column count for this graph.
+ __options__: Optional parameter for configuring other parts of the graph.
    + __className__: When sankey nodes are being drawn, drawing function will assign a CSS class for each node for you to easily style. By default it will assign `info-card` for every node. Set this parameter for different class name if you want different class.
    + __sidePadding__: Margin between columns in pixels. By default it is 60.
    + __marginBetweenDrawables__: Determines how distant the drawables (e.g Packs) will be from each other in pixels. By default it is 50.
    + __lineColor__: Connection line color. By default it is white.

### Drawable _interface_

The interface that is being drawn by Graph. You can implement this interface if you want to draw custom things inside the Graph.

#### Methods

+ __draw(context: Context, cursor: Position)__: Drawing function of the Drawable.
    + __context__: Indicates where to draw. For more information look at the Context object.
    + __cursor__: The position which indicates where this Drawable will be drawn. ALL IMPLEMENTORS HAS TO UPDATE THIS PARAMETER TO THE END COORDINATES OF THIS DRAWABLE. For more information look at the Cursor object.

### Context _class_

Most of the time you will only use this object's attributes. This object is constructed is internally, so don't construct. If you implement `Drawable` interface, in the `draw` method, you will be provided with the Context instance of Graph.

#### Attributes

+ __drawArea: HTMLElement__: Container for all.
+ __infoCardContainer: HTMLElement__: Sankey nodes' layer.
+ __canvasContainer: HTMLElement__: SVG container. For drawing connection lines etc.

### Position _class_

It is just a simple abstraction of a 2D coordinate system.

```typescript
Position(x: number, y: number)
```

#### Attributes

+ __x: number__: X coordinate value.
+ __y: number__: Y coordinate value.

#### Methods

+ __clone()__: Creates a new Position object with same attributes.

### Pack _class_

This is an implementation of `Drawable`. It represents a sankey group.

```typescript
Pack(
    grid: Array< Array<InfoCard> >, 
    connections: Map< string, Array<number[]> >, 
    config: PackConfiguration
)
```

+ __grid__: Sankey node descriptor. It describes the positions of nodes in the pack. The arrays inside the array has to be equal length as  the columnCount of Graph. To describe empty cells, it can be assigned `null`.
+ __connections__: A [string, array] map. The keys are the infocards' identifier and the value has to be an array of array of numbers.
+ __config__: A `PackConfiguration` object for configuration.

#### Methods

+ __toggleDim(state: boolean)__: Dims the pack. If the state is equal is true, it dims the packa, if false, otherwise.

### PackConfiguration _class_

This class is used to configure a Pack object.

```typescript
PackConfiguration(
    graphConfig: GraphConfiguration, 
    options?: {
        rowMargin?: number;
        connectionPointPosition?: number
    }
);
```

+ __graphConfig__: Configuration object of Pack's parent Graph.
+ __options__: An optional configuration object.
    + __rowMargin__: Distance between each row in the pack. By default it is 30.
    + __connectionPointPosition__: Vertical position of the connection point in each infocard. By default it is `null`, which means that connection points are being drawn at the vertical center of each info card.

### InfoCard _class_

This class represents a node in sankey graph.

```typescript
InfoCard(
    identifier: string, 
    content: HTMLElement, 
    config: InfocardConfig
)
```

+ __identifier__: A unique string to identify this node among other nodes.
+ __content__: Node's content as HTMLElement.
+ __config__: An InfocardConfig object to configure the infocard.

### InfocardConfig _class_

Configuration object for infocards.

```typescript
InfocardConfit(options?: { connectionPointColors?: any })
```

+ __options__: An optional options object.
    + __connectionPointColors__: Color of connection points.