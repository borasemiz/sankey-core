var SankeyGraph =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/connection-point.ts":
/*!*********************************!*\
  !*** ./src/connection-point.ts ***!
  \*********************************/
/*! exports provided: ConnectionPoint */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConnectionPoint", function() { return ConnectionPoint; });
var ConnectionPoint = /** @class */ (function () {
    function ConnectionPoint() {
    }
    return ConnectionPoint;
}());



/***/ }),

/***/ "./src/connection.ts":
/*!***************************!*\
  !*** ./src/connection.ts ***!
  \***************************/
/*! exports provided: Connection */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Connection", function() { return Connection; });
var Connection = /** @class */ (function () {
    function Connection() {
    }
    return Connection;
}());



/***/ }),

/***/ "./src/context.ts":
/*!************************!*\
  !*** ./src/context.ts ***!
  \************************/
/*! exports provided: Context */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Context", function() { return Context; });
var Context = /** @class */ (function () {
    function Context(drawArea, size) {
        this.drawArea = drawArea;
        var scrollableContent = this.prepareScrollableContent(size);
        this.canvasContainer = this.prepareCanvasElement(size);
        this.infoCardContainer = this.prepareInfocardContainer(size);
        scrollableContent.appendChild(this.canvasContainer);
        scrollableContent.appendChild(this.infoCardContainer);
        this.drawArea.appendChild(scrollableContent);
    }
    Context.prototype.clear = function () {
        this.infoCardContainer.innerHTML = "";
        this.canvasContainer.querySelector("#canvas").innerHTML = "";
    };
    Context.prototype.prepareScrollableContent = function (size) {
        var content = document.createElement("div");
        content.style.overflowY = "auto";
        content.style.overflowX = "hidden";
        content.style.width = size.width + "px";
        content.style.height = size.height + "px";
        content.style.position = "absolute";
        return content;
    };
    Context.prototype.prepareCanvasElement = function (size) {
        var element = document.createElement("div");
        element.style.width = size.width + "px";
        element.style.position = "absolute";
        element.style.backgroundColor = "transparent";
        var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        svg.setAttribute("id", "canvas");
        svg.setAttribute("width", size.width + "");
        svg.setAttribute("style", "overflow: hidden; position: relative; left: -0.25px;");
        svg.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
        element.appendChild(svg);
        return element;
    };
    Context.prototype.prepareInfocardContainer = function (size) {
        var container = document.createElement("div");
        container.style.width = size.width + "px";
        container.style.height = size.height + "px";
        container.style.position = "absolute";
        return container;
    };
    return Context;
}());



/***/ }),

/***/ "./src/graph-configuration.ts":
/*!************************************!*\
  !*** ./src/graph-configuration.ts ***!
  \************************************/
/*! exports provided: GraphConfiguration */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GraphConfiguration", function() { return GraphConfiguration; });
var GraphConfiguration = /** @class */ (function () {
    function GraphConfiguration(drawArea, columnCount, options) {
        this.drawArea = drawArea;
        this.size = { width: drawArea.offsetWidth, height: drawArea.offsetHeight };
        this.columnCount = columnCount;
        if (typeof options !== 'undefined') {
            this.className = typeof options.className !== 'undefined' ? options.className : "info-card";
            this.sidePadding = typeof options.sidePadding !== 'undefined' ? options.sidePadding : 60;
            this.marginBetweenDrawables = typeof options.marginBetweenDrawables !== 'undefined' ? options.marginBetweenDrawables : 50;
            this.lineColor = typeof options.lineColor !== 'undefined' ? options.lineColor : "white";
        }
    }
    return GraphConfiguration;
}());



/***/ }),

/***/ "./src/graph.ts":
/*!**********************!*\
  !*** ./src/graph.ts ***!
  \**********************/
/*! exports provided: Graph */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Graph", function() { return Graph; });
/* harmony import */ var _context__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./context */ "./src/context.ts");
/* harmony import */ var _position__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./position */ "./src/position.ts");


var Graph = /** @class */ (function () {
    function Graph(config, things) {
        this.configuration = config;
        this.columnCount = this.configuration.columnCount;
        this.things = things;
        this.context = new _context__WEBPACK_IMPORTED_MODULE_0__["Context"](this.configuration.drawArea, this.configuration.size);
    }
    Graph.prototype.setData = function (things) {
        this.things = things;
    };
    Graph.prototype.draw = function () {
        var _this = this;
        var cursor = new _position__WEBPACK_IMPORTED_MODULE_1__["Position"](0, this.configuration.marginBetweenDrawables);
        this.things.forEach(function (thing, index) {
            thing.draw(_this.context, cursor);
            cursor.x = 0;
            cursor.y += _this.configuration.marginBetweenDrawables;
        });
        this.resizeHeightOfContext(cursor.y);
    };
    Graph.prototype.redraw = function () {
        this.clear();
        this.draw();
    };
    Graph.prototype.redrawWithData = function (things) {
        this.setData(things);
        this.redraw();
    };
    Graph.prototype.clear = function () {
        this.context.clear();
    };
    Graph.prototype.resizeHeightOfContext = function (height) {
        this.context.infoCardContainer.style.height = height + "px";
        this.context.canvasContainer.querySelector("#canvas").setAttribute("height", height + "px");
    };
    return Graph;
}());



/***/ }),

/***/ "./src/index.ts":
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/
/*! exports provided: ConnectionPoint, Connection, Context, GraphConfiguration, InfocardConfig, InfoCard, PackConfiguration, Pack, Position, Size, Graph */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _connection_point__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./connection-point */ "./src/connection-point.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ConnectionPoint", function() { return _connection_point__WEBPACK_IMPORTED_MODULE_0__["ConnectionPoint"]; });

/* harmony import */ var _connection__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./connection */ "./src/connection.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Connection", function() { return _connection__WEBPACK_IMPORTED_MODULE_1__["Connection"]; });

/* harmony import */ var _context__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./context */ "./src/context.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Context", function() { return _context__WEBPACK_IMPORTED_MODULE_2__["Context"]; });

/* harmony import */ var _graph_configuration__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./graph-configuration */ "./src/graph-configuration.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "GraphConfiguration", function() { return _graph_configuration__WEBPACK_IMPORTED_MODULE_3__["GraphConfiguration"]; });

/* harmony import */ var _graph__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./graph */ "./src/graph.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Graph", function() { return _graph__WEBPACK_IMPORTED_MODULE_4__["Graph"]; });

/* harmony import */ var _info_card_config__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./info-card-config */ "./src/info-card-config.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "InfocardConfig", function() { return _info_card_config__WEBPACK_IMPORTED_MODULE_5__["InfocardConfig"]; });

/* harmony import */ var _info_card__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./info-card */ "./src/info-card.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "InfoCard", function() { return _info_card__WEBPACK_IMPORTED_MODULE_6__["InfoCard"]; });

/* harmony import */ var _pack_configuration__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./pack-configuration */ "./src/pack-configuration.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PackConfiguration", function() { return _pack_configuration__WEBPACK_IMPORTED_MODULE_7__["PackConfiguration"]; });

/* harmony import */ var _pack__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./pack */ "./src/pack.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Pack", function() { return _pack__WEBPACK_IMPORTED_MODULE_8__["Pack"]; });

/* harmony import */ var _position__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./position */ "./src/position.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Position", function() { return _position__WEBPACK_IMPORTED_MODULE_9__["Position"]; });

/* harmony import */ var _size__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./size */ "./src/size.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Size", function() { return _size__WEBPACK_IMPORTED_MODULE_10__["Size"]; });














/***/ }),

/***/ "./src/info-card-config.ts":
/*!*********************************!*\
  !*** ./src/info-card-config.ts ***!
  \*********************************/
/*! exports provided: InfocardConfig */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InfocardConfig", function() { return InfocardConfig; });
var InfocardConfig = /** @class */ (function () {
    function InfocardConfig(options) {
        this.colspan = 1;
        this.fillwidth = false;
        if (typeof options.connectionPointColors !== 'undefined') {
            if (typeof options.connectionPointColors === 'string') {
                this.connectionPointColors = {
                    prev: options.connectionPointColors,
                    next: options.connectionPointColors
                };
            }
            else {
                this.connectionPointColors = options.connectionPointColors;
            }
        }
    }
    return InfocardConfig;
}());



/***/ }),

/***/ "./src/info-card.ts":
/*!**************************!*\
  !*** ./src/info-card.ts ***!
  \**************************/
/*! exports provided: InfoCard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InfoCard", function() { return InfoCard; });
var InfoCard = /** @class */ (function () {
    function InfoCard(identifier, content, config) {
        this.identifier = identifier;
        this.content = content;
        this.drawn = false;
        this.connections = { prev: [], next: [] };
        this.config = config;
    }
    InfoCard.prototype.attachOnMouseHoverEventListener = function () {
        var _this = this;
        this.content.addEventListener("mouseover", function (ev) {
            _this.associatedPack.toggleDim(true);
            _this.highlightConnectedCards();
        }, false);
        this.content.addEventListener("mouseleave", function (ev) {
            _this.associatedPack.toggleDim(false);
        }, false);
    };
    InfoCard.prototype.highlightConnectedCards = function () {
        this.content.style.opacity = "1";
        this.highlighCardsNext(this.connections.next);
        this.highlightCardsPrev(this.connections.prev);
    };
    InfoCard.prototype.highlighCardsNext = function (conns) {
        var _this = this;
        conns.forEach(function (conn, index) {
            conn.path.setAttributeNS(null, "stroke-opacity", "1");
            conn.thing.content.style.opacity = "1";
            _this.highlighCardsNext(conn.thing.connections.next);
        });
    };
    InfoCard.prototype.highlightCardsPrev = function (conns) {
        var _this = this;
        conns.forEach(function (conn, index) {
            conn.path.setAttributeNS(null, "stroke-opacity", "1");
            conn.thing.content.style.opacity = "1";
            _this.highlightCardsPrev(conn.thing.connections.prev);
        });
    };
    return InfoCard;
}());



/***/ }),

/***/ "./src/pack-configuration.ts":
/*!***********************************!*\
  !*** ./src/pack-configuration.ts ***!
  \***********************************/
/*! exports provided: PackConfiguration */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PackConfiguration", function() { return PackConfiguration; });
var PackConfiguration = /** @class */ (function () {
    function PackConfiguration(drawingMode, graphConfig, options) {
        this.drawingMode = drawingMode;
        this.infocardClassName = graphConfig.className;
        this.sidePadding = graphConfig.sidePadding;
        this.columnCount = graphConfig.columnCount;
        this.columnWidth = Math.floor(graphConfig.size.width / graphConfig.columnCount);
        this.lineColor = graphConfig.lineColor;
        if (typeof options !== 'undefined') {
            this.rowMargin = typeof options.rowMargin === 'undefined' ? 30 : options.rowMargin;
            this.connectionPointPosition = typeof options.connectionPointPosition === 'undefined' ? null : options.connectionPointPosition;
        }
    }
    return PackConfiguration;
}());



/***/ }),

/***/ "./src/pack.ts":
/*!*********************!*\
  !*** ./src/pack.ts ***!
  \*********************/
/*! exports provided: Pack */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Pack", function() { return Pack; });
/* harmony import */ var _position__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./position */ "./src/position.ts");

var Pack = /** @class */ (function () {
    function Pack(grid, connections, config) {
        this.grid = grid;
        this.connections = connections;
        this.config = config;
        this.infocardWidth = this.config.columnWidth - (2 * this.config.sidePadding);
    }
    Pack.prototype.setConfig = function (config) {
        this.config = config;
    };
    Pack.prototype.draw = function (context, cursor) {
        this.putInfocards(context, cursor, this.config.drawingMode);
        this.connectInfocards(context);
        this.context = context;
    };
    Pack.prototype.toggleDim = function (state) {
        var opacity = state ? 0.5 : 1.0;
        var svgElement = this.context.canvasContainer.querySelector("#canvas");
        for (var i = 0; i < svgElement.children.length; i++) {
            svgElement.children[i].setAttributeNS(null, "stroke-opacity", opacity + "");
        }
        for (var i = 0; i < this.context.infoCardContainer.children.length; i++) {
            this.context.infoCardContainer.childNodes[i].style.opacity = opacity + "";
        }
    };
    Pack.prototype.connectInfocards = function (context) {
        var _this = this;
        this.grid.forEach(function (columns, rowIndex) {
            columns.forEach(function (cell, columnIndex) {
                if (cell === null)
                    return;
                var connection = _this.connections.get(cell.identifier);
                if (connection === null || connection === undefined)
                    return;
                connection.forEach(function (conn, index) {
                    _this.connectTwoInfoCards(cell, _this.grid[conn[0]][conn[1]], context);
                });
            });
        });
    };
    Pack.prototype.putInfocards = function (context, cursor, mode) {
        switch (mode) {
            case "compact":
                this.putInfocardsInCompact(context, cursor);
                break;
            case "loose":
            default:
                this.putInfocardsInLoose(context, cursor);
                break;
        }
    };
    Pack.prototype.putInfocardsInCompact = function (context, cursor) {
        var _this = this;
        var maxHeight = 0, tempCursorY = cursor.y;
        var _loop_1 = function (columnIndex) {
            cursor.x = this_1.config.sidePadding + (this_1.config.columnWidth * columnIndex);
            cursor.y = tempCursorY;
            var totalHeight = 0;
            this_1.grid.forEach(function (columns) {
                if (columns[columnIndex] === null)
                    return;
                var height = _this.drawInfocard(cursor, context, columns[columnIndex]);
                columns[columnIndex].associatedPack = _this;
                columns[columnIndex].attachOnMouseHoverEventListener();
                cursor.y += height + _this.config.rowMargin;
                totalHeight += height + _this.config.rowMargin;
            });
            maxHeight = totalHeight > maxHeight ? totalHeight : maxHeight;
        };
        var this_1 = this;
        for (var columnIndex = 0; columnIndex < this.config.columnCount; columnIndex++) {
            _loop_1(columnIndex);
        }
        cursor.y = maxHeight;
    };
    Pack.prototype.putInfocardsInLoose = function (context, cursor) {
        var _this = this;
        this.grid.forEach(function (columns, rowIndex) {
            var maxHeight = 0;
            cursor.x = _this.config.sidePadding;
            columns.forEach(function (card, columnIndex) {
                if (card === null) {
                    cursor.x += _this.config.columnWidth;
                    return;
                }
                var height = _this.drawInfocard(cursor, context, card);
                cursor.x += _this.config.columnWidth;
                maxHeight = maxHeight < height ? height : maxHeight;
                card.associatedPack = _this;
                card.attachOnMouseHoverEventListener();
            });
            cursor.y += maxHeight + _this.config.rowMargin;
        });
    };
    Pack.prototype.drawInfocard = function (cursor, context, card) {
        card.content.style.position = "absolute";
        card.content.className = this.config.infocardClassName + " " + card.content.className;
        card.content.style.width = this.infocardWidth + "px";
        card.content.style.top = cursor.y + "px";
        card.content.style.left = cursor.x + "px";
        card.content.style.boxSizing = "border-box";
        card.content.style.transition = "opacity 0.2s linear";
        context.infoCardContainer.appendChild(card.content);
        card.drawn = true;
        //cursor.x += this.config.columnWidth;
        return card.content.offsetHeight;
    };
    Pack.prototype.connectTwoInfoCards = function (start, end, context) {
        var startY = this.config.connectionPointPosition === null ?
            Math.floor(start.content.offsetHeight / 2) : this.config.connectionPointPosition;
        var sPosition = new _position__WEBPACK_IMPORTED_MODULE_0__["Position"](parseInt(start.content.style.left.replace("px", "")) + start.content.offsetWidth, parseInt(start.content.style.top.replace("px", "")) + startY);
        var endY = this.config.connectionPointPosition === null ?
            Math.floor(end.content.offsetHeight / 2) : this.config.connectionPointPosition;
        var ePosition = new _position__WEBPACK_IMPORTED_MODULE_0__["Position"](parseInt(end.content.style.left.replace("px", "")), parseInt(end.content.style.top.replace("px", "")) + endY);
        var svg = context.canvasContainer.querySelector("#canvas");
        var path = this.drawCurvedLine(sPosition, ePosition, svg);
        this.putConnectionPoint(start, "start");
        this.putConnectionPoint(end, "end");
        start.connections.next.push({ path: path, thing: end });
        end.connections.prev.push({ path: path, thing: start });
    };
    Pack.prototype.drawCurvedLine = function (start, end, svg, debugMode) {
        if (debugMode === void 0) { debugMode = false; }
        var pathElement = document.createElementNS("http://www.w3.org/2000/svg", "path");
        var xCenter = start.x + Math.floor((end.x - start.x) / 2);
        var pathString = "M " + start.x + "," + start.y + " C " + xCenter + "," + start.y + " " + xCenter + "," + end.y +
            " " + end.x + "," + end.y;
        pathElement.setAttributeNS(null, "d", pathString);
        pathElement.setAttributeNS(null, "fill", "transparent");
        pathElement.setAttributeNS(null, "stroke", this.config.lineColor);
        pathElement.setAttributeNS(null, "stroke-width", "2");
        pathElement.style.transition = "stroke-opacity 0.2s linear";
        svg.appendChild(pathElement);
        return pathElement;
    };
    Pack.prototype.putConnectionPoint = function (infocard, where) {
        var point = document.createElement("div");
        point.style.boxSizing = "border-box";
        point.style.width = "12px";
        point.style.height = "12px";
        point.style.position = "absolute";
        point.style.borderRadius = "50%";
        point.style.backgroundColor = "black";
        var yPos = this.config.connectionPointPosition === null ?
            Math.floor(infocard.content.offsetHeight / 2) : this.config.connectionPointPosition;
        switch (where) {
            case "end":
                point.style.left = "-6px";
                point.style.backgroundColor = infocard.config.connectionPointColors.next;
                point.style.border = "1px solid " + infocard.config.connectionPointColors.next;
                break;
            default:
                point.style.right = "-6px";
                point.style.backgroundColor = infocard.config.connectionPointColors.prev;
                point.style.border = "1px solid " + infocard.config.connectionPointColors.prev;
                break;
        }
        point.style.top = (yPos - 6) + "px";
        infocard.content.appendChild(point);
    };
    return Pack;
}());



/***/ }),

/***/ "./src/position.ts":
/*!*************************!*\
  !*** ./src/position.ts ***!
  \*************************/
/*! exports provided: Position */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Position", function() { return Position; });
var Position = /** @class */ (function () {
    function Position(x, y) {
        this.x = x;
        this.y = y;
    }
    Position.prototype.clone = function () {
        return new Position(this.x, this.y);
    };
    return Position;
}());



/***/ }),

/***/ "./src/size.ts":
/*!*********************!*\
  !*** ./src/size.ts ***!
  \*********************/
/*! exports provided: Size */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Size", function() { return Size; });
var Size = /** @class */ (function () {
    function Size() {
    }
    return Size;
}());



/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9TYW5rZXlHcmFwaC93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly9TYW5rZXlHcmFwaC8uL3NyYy9jb25uZWN0aW9uLXBvaW50LnRzIiwid2VicGFjazovL1NhbmtleUdyYXBoLy4vc3JjL2Nvbm5lY3Rpb24udHMiLCJ3ZWJwYWNrOi8vU2Fua2V5R3JhcGgvLi9zcmMvY29udGV4dC50cyIsIndlYnBhY2s6Ly9TYW5rZXlHcmFwaC8uL3NyYy9ncmFwaC1jb25maWd1cmF0aW9uLnRzIiwid2VicGFjazovL1NhbmtleUdyYXBoLy4vc3JjL2dyYXBoLnRzIiwid2VicGFjazovL1NhbmtleUdyYXBoLy4vc3JjL2luZGV4LnRzIiwid2VicGFjazovL1NhbmtleUdyYXBoLy4vc3JjL2luZm8tY2FyZC1jb25maWcudHMiLCJ3ZWJwYWNrOi8vU2Fua2V5R3JhcGgvLi9zcmMvaW5mby1jYXJkLnRzIiwid2VicGFjazovL1NhbmtleUdyYXBoLy4vc3JjL3BhY2stY29uZmlndXJhdGlvbi50cyIsIndlYnBhY2s6Ly9TYW5rZXlHcmFwaC8uL3NyYy9wYWNrLnRzIiwid2VicGFjazovL1NhbmtleUdyYXBoLy4vc3JjL3Bvc2l0aW9uLnRzIiwid2VicGFjazovL1NhbmtleUdyYXBoLy4vc3JjL3NpemUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtEQUEwQyxnQ0FBZ0M7QUFDMUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnRUFBd0Qsa0JBQWtCO0FBQzFFO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUF5QyxpQ0FBaUM7QUFDMUUsd0hBQWdILG1CQUFtQixFQUFFO0FBQ3JJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0FDaEZBO0FBQUE7SUFBQTtJQUdBLENBQUM7SUFBRCxzQkFBQztBQUFELENBQUM7Ozs7Ozs7Ozs7Ozs7OztBQ0hEO0FBQUE7SUFBQTtJQUdBLENBQUM7SUFBRCxpQkFBQztBQUFELENBQUM7Ozs7Ozs7Ozs7Ozs7OztBQ0hEO0FBQUE7SUFLRSxpQkFBWSxRQUFxQixFQUFFLElBQVU7UUFDM0MsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFDekIsSUFBSSxpQkFBaUIsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUU3RCxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ3BELGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUN0RCxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFFTSx1QkFBSyxHQUFaO1FBQ0UsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7UUFDdEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztJQUMvRCxDQUFDO0lBRU8sMENBQXdCLEdBQWhDLFVBQWlDLElBQVU7UUFDekMsSUFBSSxPQUFPLEdBQWdCLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDekQsT0FBTyxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsTUFBTSxDQUFDO1FBQ2pDLE9BQU8sQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQztRQUNuQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztRQUN4QyxPQUFPLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUMxQyxPQUFPLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUM7UUFDcEMsT0FBTyxPQUFPLENBQUM7SUFDakIsQ0FBQztJQUVPLHNDQUFvQixHQUE1QixVQUE2QixJQUFVO1FBQ3JDLElBQUksT0FBTyxHQUFnQixRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3pELE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ3hDLE9BQU8sQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLFVBQVUsQ0FBQztRQUNwQyxPQUFPLENBQUMsS0FBSyxDQUFDLGVBQWUsR0FBRyxhQUFhLENBQUM7UUFFOUMsSUFBSSxHQUFHLEdBQWUsUUFBUSxDQUFDLGVBQWUsQ0FBQyw0QkFBNEIsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNwRixHQUFHLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQztRQUNqQyxHQUFHLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQzNDLEdBQUcsQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFFLHNEQUFzRCxDQUFDLENBQUM7UUFDbEYsR0FBRyxDQUFDLGNBQWMsQ0FBQywrQkFBK0IsRUFBRSxhQUFhLEVBQUUsOEJBQThCLENBQUMsQ0FBQztRQUNuRyxPQUFPLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRXpCLE9BQU8sT0FBTyxDQUFDO0lBQ2pCLENBQUM7SUFFTywwQ0FBd0IsR0FBaEMsVUFBaUMsSUFBVTtRQUN6QyxJQUFJLFNBQVMsR0FBZ0IsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMzRCxTQUFTLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztRQUMxQyxTQUFTLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUM1QyxTQUFTLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUM7UUFDdEMsT0FBTyxTQUFTLENBQUM7SUFDbkIsQ0FBQztJQUNILGNBQUM7QUFBRCxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7QUN0REQ7QUFBQTtJQVNFLDRCQUFZLFFBQXFCLEVBQy9CLFdBQW1CLEVBQ25CLE9BS0M7UUFHRCxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztRQUN6QixJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsS0FBSyxFQUFFLFFBQVEsQ0FBQyxXQUFXLEVBQUUsTUFBTSxFQUFFLFFBQVEsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUMzRSxJQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztRQUMvQixJQUFJLE9BQU8sT0FBTyxLQUFLLFdBQVcsRUFBRTtZQUNsQyxJQUFJLENBQUMsU0FBUyxHQUFHLE9BQU8sT0FBTyxDQUFDLFNBQVMsS0FBSyxXQUFXLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQztZQUM1RixJQUFJLENBQUMsV0FBVyxHQUFHLE9BQU8sT0FBTyxDQUFDLFdBQVcsS0FBSyxXQUFXLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztZQUN6RixJQUFJLENBQUMsc0JBQXNCLEdBQUcsT0FBTyxPQUFPLENBQUMsc0JBQXNCLEtBQUssV0FBVyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztZQUMxSCxJQUFJLENBQUMsU0FBUyxHQUFHLE9BQU8sT0FBTyxDQUFDLFNBQVMsS0FBSyxXQUFXLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQztTQUN6RjtJQUNILENBQUM7SUFDSCx5QkFBQztBQUFELENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdCbUM7QUFDRTtBQUV0QztJQVNFLGVBQVksTUFBMEIsRUFBRSxNQUF1QjtRQUM3RCxJQUFJLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQztRQUM1QixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDO1FBQ2xELElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxnREFBTyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDbkYsQ0FBQztJQUVNLHVCQUFPLEdBQWQsVUFBZSxNQUF1QjtRQUNwQyxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztJQUN2QixDQUFDO0lBRU0sb0JBQUksR0FBWDtRQUFBLGlCQVFDO1FBUEMsSUFBSSxNQUFNLEdBQWEsSUFBSSxrREFBUSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLHNCQUFzQixDQUFDLENBQUM7UUFDbEYsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBQyxLQUFlLEVBQUUsS0FBYTtZQUNqRCxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDakMsTUFBTSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDYixNQUFNLENBQUMsQ0FBQyxJQUFJLEtBQUksQ0FBQyxhQUFhLENBQUMsc0JBQXNCLENBQUM7UUFDeEQsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFFTSxzQkFBTSxHQUFiO1FBQ0UsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2IsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2QsQ0FBQztJQUVNLDhCQUFjLEdBQXJCLFVBQXNCLE1BQXVCO1FBQzNDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDckIsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ2hCLENBQUM7SUFFTyxxQkFBSyxHQUFiO1FBQ0UsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBRU8scUNBQXFCLEdBQTdCLFVBQThCLE1BQWM7UUFDMUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDNUQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsTUFBTSxHQUFHLElBQUksQ0FBQyxDQUFDO0lBQzlGLENBQUM7SUFDSCxZQUFDO0FBQUQsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDckRrQztBQUNOO0FBQ0g7QUFFWTtBQUNkO0FBQ1c7QUFDUDtBQUNTO0FBQ2Q7QUFDSTtBQUNKOzs7Ozs7Ozs7Ozs7OztBQ1h2QjtBQUFBO0lBUUUsd0JBQVksT0FFWDtRQUNDLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1FBQ2pCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLElBQUksT0FBTyxPQUFPLENBQUMscUJBQXFCLEtBQUssV0FBVyxFQUFFO1lBQ3hELElBQUksT0FBTyxPQUFPLENBQUMscUJBQXFCLEtBQUssUUFBUSxFQUFFO2dCQUNyRCxJQUFJLENBQUMscUJBQXFCLEdBQUc7b0JBQzNCLElBQUksRUFBRSxPQUFPLENBQUMscUJBQXFCO29CQUNuQyxJQUFJLEVBQUUsT0FBTyxDQUFDLHFCQUFxQjtpQkFDcEM7YUFDRjtpQkFBTTtnQkFDTCxJQUFJLENBQUMscUJBQXFCLEdBQUcsT0FBTyxDQUFDLHFCQUFxQixDQUFDO2FBQzVEO1NBQ0Y7SUFDSCxDQUFDO0lBQ0gscUJBQUM7QUFBRCxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7QUNwQkQ7QUFBQTtJQVdFLGtCQUFZLFVBQWtCLEVBQUUsT0FBb0IsRUFBRSxNQUFzQjtRQUMxRSxJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztRQUM3QixJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztRQUN2QixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNuQixJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLENBQUM7UUFDMUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7SUFDdkIsQ0FBQztJQUVNLGtEQUErQixHQUF0QztRQUFBLGlCQVFDO1FBUEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsVUFBQyxFQUFjO1lBQ3hELEtBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3BDLEtBQUksQ0FBQyx1QkFBdUIsRUFBRSxDQUFDO1FBQ2pDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNWLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxFQUFFLFVBQUMsRUFBYztZQUN6RCxLQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN2QyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDWixDQUFDO0lBRU8sMENBQXVCLEdBQS9CO1FBQ0UsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztRQUNqQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNqRCxDQUFDO0lBRU8sb0NBQWlCLEdBQXpCLFVBQTBCLEtBQXdCO1FBQWxELGlCQU1DO1FBTEMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQWdCLEVBQUUsS0FBYTtZQUM1QyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsZ0JBQWdCLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDdEQsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUM7WUFDdkMsS0FBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RELENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLHFDQUFrQixHQUExQixVQUEyQixLQUF3QjtRQUFuRCxpQkFNQztRQUxDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFnQixFQUFFLEtBQWE7WUFDNUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLGdCQUFnQixFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ3RELElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1lBQ3ZDLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN2RCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDSCxlQUFDO0FBQUQsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7O0FDcEREO0FBQUE7SUFVRSwyQkFBWSxXQUFtQixFQUFFLFdBQStCLEVBQUUsT0FHakU7UUFDQyxJQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztRQUMvQixJQUFJLENBQUMsaUJBQWlCLEdBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQztRQUMvQyxJQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQyxXQUFXLENBQUM7UUFDM0MsSUFBSSxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUMsV0FBVyxDQUFDO1FBQzNDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDaEYsSUFBSSxDQUFDLFNBQVMsR0FBRyxXQUFXLENBQUMsU0FBUyxDQUFDO1FBQ3ZDLElBQUksT0FBTyxPQUFPLEtBQUssV0FBVyxFQUFFO1lBQ2xDLElBQUksQ0FBQyxTQUFTLEdBQUcsT0FBTyxPQUFPLENBQUMsU0FBUyxLQUFLLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDO1lBQ25GLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxPQUFPLE9BQU8sQ0FBQyx1QkFBdUIsS0FBSyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDO1NBQ2hJO0lBQ0gsQ0FBQztJQUNILHdCQUFDO0FBQUQsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN2QnFDO0FBRXRDO0lBT0UsY0FBWSxJQUE0QixFQUFFLFdBQXlDLEVBQUUsTUFBMEI7UUFDN0csSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDakIsSUFBSSxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUM7UUFDL0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDckIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQy9FLENBQUM7SUFFTSx3QkFBUyxHQUFoQixVQUFpQixNQUF5QjtRQUN4QyxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztJQUN2QixDQUFDO0lBRU0sbUJBQUksR0FBWCxVQUFZLE9BQWdCLEVBQUUsTUFBZ0I7UUFDNUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQy9CLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO0lBQ3pCLENBQUM7SUFFTSx3QkFBUyxHQUFoQixVQUFpQixLQUFjO1FBQzdCLElBQUksT0FBTyxHQUFXLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7UUFDeEMsSUFBSSxVQUFVLEdBQWUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ25GLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxVQUFVLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNuRCxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsZ0JBQWdCLEVBQUUsT0FBTyxHQUFHLEVBQUUsQ0FBQyxDQUFDO1NBQzdFO1FBQ0QsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN6RCxJQUFJLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUUsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE9BQU8sR0FBRyxFQUFFLENBQUM7U0FDMUY7SUFDSCxDQUFDO0lBRU8sK0JBQWdCLEdBQXhCLFVBQXlCLE9BQWdCO1FBQXpDLGlCQVdDO1FBVkMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBQyxPQUF3QixFQUFFLFFBQWdCO1lBQzNELE9BQU8sQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFjLEVBQUUsV0FBbUI7Z0JBQ2xELElBQUksSUFBSSxLQUFLLElBQUk7b0JBQUUsT0FBTztnQkFDMUIsSUFBSSxVQUFVLEdBQXlCLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDN0UsSUFBSSxVQUFVLEtBQUssSUFBSSxJQUFJLFVBQVUsS0FBSyxTQUFTO29CQUFFLE9BQU87Z0JBQzVELFVBQVUsQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFtQixFQUFFLEtBQWE7b0JBQ3BELEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEVBQUUsS0FBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQztnQkFDdkUsQ0FBQyxDQUFDLENBQUM7WUFDTCxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLDJCQUFZLEdBQXBCLFVBQXFCLE9BQWdCLEVBQUUsTUFBZ0IsRUFBRSxJQUFZO1FBQ25FLFFBQVEsSUFBSSxFQUFFO1lBQ1osS0FBSyxTQUFTO2dCQUFFLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUM7Z0JBQUMsTUFBTTtZQUNuRSxLQUFLLE9BQU8sQ0FBQztZQUNiO2dCQUNFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUM7Z0JBQUMsTUFBTTtTQUNwRDtJQUVILENBQUM7SUFFTyxvQ0FBcUIsR0FBN0IsVUFBOEIsT0FBZ0IsRUFBRSxNQUFnQjtRQUFoRSxpQkFpQkM7UUFoQkMsSUFBSSxTQUFTLEdBQVcsQ0FBQyxFQUFFLFdBQVcsR0FBVyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dDQUNqRCxXQUFXO1lBQ2xCLE1BQU0sQ0FBQyxDQUFDLEdBQUcsT0FBSyxNQUFNLENBQUMsV0FBVyxHQUFHLENBQUMsT0FBSyxNQUFNLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQyxDQUFDO1lBQzdFLE1BQU0sQ0FBQyxDQUFDLEdBQUcsV0FBVyxDQUFDO1lBQ3ZCLElBQUksV0FBVyxHQUFXLENBQUMsQ0FBQztZQUM1QixPQUFLLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBQyxPQUF3QjtnQkFDekMsSUFBSSxPQUFPLENBQUMsV0FBVyxDQUFDLEtBQUssSUFBSTtvQkFBRSxPQUFPO2dCQUMxQyxJQUFJLE1BQU0sR0FBVyxLQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxPQUFPLEVBQUUsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7Z0JBQzlFLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQyxjQUFjLEdBQUcsS0FBSSxDQUFDO2dCQUMzQyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUMsK0JBQStCLEVBQUUsQ0FBQztnQkFDdkQsTUFBTSxDQUFDLENBQUMsSUFBSSxNQUFNLEdBQUcsS0FBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUM7Z0JBQzNDLFdBQVcsSUFBSSxNQUFNLEdBQUcsS0FBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUM7WUFDaEQsQ0FBQyxDQUFDLENBQUM7WUFDSCxTQUFTLEdBQUcsV0FBVyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7UUFDaEUsQ0FBQzs7UUFiRCxLQUFLLElBQUksV0FBVyxHQUFHLENBQUMsRUFBRSxXQUFXLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsV0FBVyxFQUFFO29CQUFyRSxXQUFXO1NBYW5CO1FBQ0QsTUFBTSxDQUFDLENBQUMsR0FBRyxTQUFTLENBQUM7SUFDdkIsQ0FBQztJQUVPLGtDQUFtQixHQUEzQixVQUE0QixPQUFnQixFQUFFLE1BQWdCO1FBQTlELGlCQWlCQztRQWhCQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFDLE9BQXdCLEVBQUUsUUFBZ0I7WUFDM0QsSUFBSSxTQUFTLEdBQVcsQ0FBQyxDQUFDO1lBQzFCLE1BQU0sQ0FBQyxDQUFDLEdBQUcsS0FBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUM7WUFDbkMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQWMsRUFBRSxXQUFtQjtnQkFDbEQsSUFBSSxJQUFJLEtBQUssSUFBSSxFQUFFO29CQUNqQixNQUFNLENBQUMsQ0FBQyxJQUFJLEtBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDO29CQUNwQyxPQUFPO2lCQUNSO2dCQUNELElBQUksTUFBTSxHQUFXLEtBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDOUQsTUFBTSxDQUFDLENBQUMsSUFBSSxLQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQztnQkFDcEMsU0FBUyxHQUFHLFNBQVMsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO2dCQUNwRCxJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUksQ0FBQztnQkFDM0IsSUFBSSxDQUFDLCtCQUErQixFQUFFLENBQUM7WUFDekMsQ0FBQyxDQUFDLENBQUM7WUFDSCxNQUFNLENBQUMsQ0FBQyxJQUFJLFNBQVMsR0FBRyxLQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQztRQUNoRCxDQUFDLENBQUM7SUFDSixDQUFDO0lBRU8sMkJBQVksR0FBcEIsVUFBcUIsTUFBZ0IsRUFBRSxPQUFnQixFQUFFLElBQWM7UUFDckUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLFVBQVUsQ0FBQztRQUN6QyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLGlCQUFpQixHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQztRQUN0RixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7UUFDckQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLE1BQU0sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDO1FBQ3pDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxNQUFNLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQztRQUMxQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsWUFBWSxDQUFDO1FBQzVDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxxQkFBcUIsQ0FBQztRQUN0RCxPQUFPLENBQUMsaUJBQWlCLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNwRCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztRQUVsQixzQ0FBc0M7UUFDdEMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQztJQUNuQyxDQUFDO0lBRU8sa0NBQW1CLEdBQTNCLFVBQTRCLEtBQWUsRUFBRSxHQUFhLEVBQUUsT0FBZ0I7UUFDMUUsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyx1QkFBdUIsS0FBSyxJQUFJLENBQUMsQ0FBQztZQUN6RCxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLHVCQUF1QixDQUFDO1FBQ25GLElBQUksU0FBUyxHQUFhLElBQUksa0RBQVEsQ0FDcEMsUUFBUSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQ2hGLFFBQVEsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FDN0QsQ0FBQztRQUVGLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsdUJBQXVCLEtBQUssSUFBSSxDQUFDLENBQUM7WUFDdkQsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyx1QkFBdUIsQ0FBQztRQUNqRixJQUFJLFNBQVMsR0FBYSxJQUFJLGtEQUFRLENBQ3BDLFFBQVEsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQyxFQUNsRCxRQUFRLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQ3pELENBQUM7UUFFRixJQUFJLEdBQUcsR0FBZSxPQUFPLENBQUMsZUFBZSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUN2RSxJQUFJLElBQUksR0FBbUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLEVBQUUsU0FBUyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQzFFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDeEMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNwQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQ3hELEdBQUcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7SUFDMUQsQ0FBQztJQUVPLDZCQUFjLEdBQXRCLFVBQXVCLEtBQWUsRUFBRSxHQUFhLEVBQUUsR0FBZSxFQUFFLFNBQTBCO1FBQTFCLDZDQUEwQjtRQUNoRyxJQUFJLFdBQVcsR0FBbUIsUUFBUSxDQUFDLGVBQWUsQ0FBQyw0QkFBNEIsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUNqRyxJQUFJLE9BQU8sR0FBVyxLQUFLLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUVsRSxJQUFJLFVBQVUsR0FBRyxJQUFJLEdBQUcsS0FBSyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsS0FBSyxDQUFDLENBQUMsR0FBRyxLQUFLLEdBQUcsT0FBTyxHQUFHLEdBQUcsR0FBRyxLQUFLLENBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBRyxPQUFPLEdBQUcsR0FBRyxHQUFHLEdBQUcsQ0FBQyxDQUFDO1lBQzdHLEdBQUcsR0FBRyxHQUFHLENBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQzVCLFdBQVcsQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLEdBQUcsRUFBRSxVQUFVLENBQUMsQ0FBQztRQUNsRCxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxNQUFNLEVBQUUsYUFBYSxDQUFDLENBQUM7UUFDeEQsV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDbEUsV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsY0FBYyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ3RELFdBQVcsQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLDRCQUE0QixDQUFDO1FBQzVELEdBQUcsQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUM7UUFFN0IsT0FBTyxXQUFXLENBQUM7SUFDckIsQ0FBQztJQUVPLGlDQUFrQixHQUExQixVQUEyQixRQUFrQixFQUFFLEtBQWE7UUFDMUQsSUFBSSxLQUFLLEdBQW1CLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDMUQsS0FBSyxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsWUFBWSxDQUFDO1FBQ3JDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQztRQUMzQixLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDNUIsS0FBSyxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDO1FBQ2xDLEtBQUssQ0FBQyxLQUFLLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUNqQyxLQUFLLENBQUMsS0FBSyxDQUFDLGVBQWUsR0FBRyxPQUFPLENBQUM7UUFFdEMsSUFBSSxJQUFJLEdBQVcsSUFBSSxDQUFDLE1BQU0sQ0FBQyx1QkFBdUIsS0FBSyxJQUFJLENBQUMsQ0FBQztZQUMvRCxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLHVCQUF1QixDQUFDO1FBQ3RGLFFBQVEsS0FBSyxFQUFFO1lBQ2IsS0FBSyxLQUFLO2dCQUNSLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQztnQkFDMUIsS0FBSyxDQUFDLEtBQUssQ0FBQyxlQUFlLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUM7Z0JBQ3pFLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLFlBQVksR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQztnQkFDL0UsTUFBTTtZQUNSO2dCQUNFLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQztnQkFDM0IsS0FBSyxDQUFDLEtBQUssQ0FBQyxlQUFlLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUM7Z0JBQ3pFLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLFlBQVksR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQztnQkFDL0UsTUFBTTtTQUNUO1FBQ0QsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDO1FBQ3BDLFFBQVEsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3RDLENBQUM7SUFDSCxXQUFDO0FBQUQsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7O0FDdExEO0FBQUE7SUFRRSxrQkFBWSxDQUFTLEVBQUUsQ0FBUztRQUM5QixJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNYLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ2IsQ0FBQztJQVBNLHdCQUFLLEdBQVo7UUFDRSxPQUFPLElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3RDLENBQUM7SUFNSCxlQUFDO0FBQUQsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7O0FDWkQ7QUFBQTtJQUFBO0lBR0EsQ0FBQztJQUFELFdBQUM7QUFBRCxDQUFDIiwiZmlsZSI6ImJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL2luZGV4LnRzXCIpO1xuIiwiaW1wb3J0IHsgUG9zaXRpb24gfSBmcm9tICcuL3Bvc2l0aW9uJztcblxuZXhwb3J0IGNsYXNzIENvbm5lY3Rpb25Qb2ludCB7XG4gIHBvc2l0aW9uOiBQb3NpdGlvbjtcbiAgZWxlbWVudDogSFRNTEVsZW1lbnQ7XG59IiwiaW1wb3J0IHsgSW5mb0NhcmQgfSBmcm9tICcuL2luZm8tY2FyZCc7XG5cbmV4cG9ydCBjbGFzcyBDb25uZWN0aW9uIHtcbiAgcGF0aDogU1ZHUGF0aEVsZW1lbnQ7XG4gIHRoaW5nOiBJbmZvQ2FyZDtcbn0iLCJpbXBvcnQgeyBTaXplIH0gZnJvbSBcIi4vc2l6ZVwiO1xuXG5leHBvcnQgY2xhc3MgQ29udGV4dCB7XG4gIGRyYXdBcmVhOiBIVE1MRWxlbWVudDtcbiAgaW5mb0NhcmRDb250YWluZXI6IEhUTUxFbGVtZW50O1xuICBjYW52YXNDb250YWluZXI6IEhUTUxFbGVtZW50O1xuXG4gIGNvbnN0cnVjdG9yKGRyYXdBcmVhOiBIVE1MRWxlbWVudCwgc2l6ZTogU2l6ZSkge1xuICAgIHRoaXMuZHJhd0FyZWEgPSBkcmF3QXJlYTtcbiAgICBsZXQgc2Nyb2xsYWJsZUNvbnRlbnQgPSB0aGlzLnByZXBhcmVTY3JvbGxhYmxlQ29udGVudChzaXplKTtcbiAgICB0aGlzLmNhbnZhc0NvbnRhaW5lciA9IHRoaXMucHJlcGFyZUNhbnZhc0VsZW1lbnQoc2l6ZSk7XG4gICAgdGhpcy5pbmZvQ2FyZENvbnRhaW5lciA9IHRoaXMucHJlcGFyZUluZm9jYXJkQ29udGFpbmVyKHNpemUpO1xuXG4gICAgc2Nyb2xsYWJsZUNvbnRlbnQuYXBwZW5kQ2hpbGQodGhpcy5jYW52YXNDb250YWluZXIpO1xuICAgIHNjcm9sbGFibGVDb250ZW50LmFwcGVuZENoaWxkKHRoaXMuaW5mb0NhcmRDb250YWluZXIpO1xuICAgIHRoaXMuZHJhd0FyZWEuYXBwZW5kQ2hpbGQoc2Nyb2xsYWJsZUNvbnRlbnQpO1xuICB9XG5cbiAgcHVibGljIGNsZWFyKCk6IHZvaWQge1xuICAgIHRoaXMuaW5mb0NhcmRDb250YWluZXIuaW5uZXJIVE1MID0gXCJcIjtcbiAgICB0aGlzLmNhbnZhc0NvbnRhaW5lci5xdWVyeVNlbGVjdG9yKFwiI2NhbnZhc1wiKS5pbm5lckhUTUwgPSBcIlwiO1xuICB9XG5cbiAgcHJpdmF0ZSBwcmVwYXJlU2Nyb2xsYWJsZUNvbnRlbnQoc2l6ZTogU2l6ZSk6IEhUTUxFbGVtZW50IHtcbiAgICBsZXQgY29udGVudDogSFRNTEVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO1xuICAgIGNvbnRlbnQuc3R5bGUub3ZlcmZsb3dZID0gXCJhdXRvXCI7XG4gICAgY29udGVudC5zdHlsZS5vdmVyZmxvd1ggPSBcImhpZGRlblwiO1xuICAgIGNvbnRlbnQuc3R5bGUud2lkdGggPSBzaXplLndpZHRoICsgXCJweFwiO1xuICAgIGNvbnRlbnQuc3R5bGUuaGVpZ2h0ID0gc2l6ZS5oZWlnaHQgKyBcInB4XCI7XG4gICAgY29udGVudC5zdHlsZS5wb3NpdGlvbiA9IFwiYWJzb2x1dGVcIjtcbiAgICByZXR1cm4gY29udGVudDtcbiAgfVxuXG4gIHByaXZhdGUgcHJlcGFyZUNhbnZhc0VsZW1lbnQoc2l6ZTogU2l6ZSk6IEhUTUxFbGVtZW50IHtcbiAgICBsZXQgZWxlbWVudDogSFRNTEVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO1xuICAgIGVsZW1lbnQuc3R5bGUud2lkdGggPSBzaXplLndpZHRoICsgXCJweFwiO1xuICAgIGVsZW1lbnQuc3R5bGUucG9zaXRpb24gPSBcImFic29sdXRlXCI7XG4gICAgZWxlbWVudC5zdHlsZS5iYWNrZ3JvdW5kQ29sb3IgPSBcInRyYW5zcGFyZW50XCI7XG5cbiAgICBsZXQgc3ZnOiBTVkdFbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudE5TKFwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIiwgXCJzdmdcIik7XG4gICAgc3ZnLnNldEF0dHJpYnV0ZShcImlkXCIsIFwiY2FudmFzXCIpO1xuICAgIHN2Zy5zZXRBdHRyaWJ1dGUoXCJ3aWR0aFwiLCBzaXplLndpZHRoICsgXCJcIik7XG4gICAgc3ZnLnNldEF0dHJpYnV0ZShcInN0eWxlXCIsIFwib3ZlcmZsb3c6IGhpZGRlbjsgcG9zaXRpb246IHJlbGF0aXZlOyBsZWZ0OiAtMC4yNXB4O1wiKTtcbiAgICBzdmcuc2V0QXR0cmlidXRlTlMoXCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3htbG5zL1wiLCBcInhtbG5zOnhsaW5rXCIsIFwiaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGlua1wiKTtcbiAgICBlbGVtZW50LmFwcGVuZENoaWxkKHN2Zyk7XG5cbiAgICByZXR1cm4gZWxlbWVudDtcbiAgfVxuXG4gIHByaXZhdGUgcHJlcGFyZUluZm9jYXJkQ29udGFpbmVyKHNpemU6IFNpemUpOiBIVE1MRWxlbWVudCB7XG4gICAgbGV0IGNvbnRhaW5lcjogSFRNTEVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO1xuICAgIGNvbnRhaW5lci5zdHlsZS53aWR0aCA9IHNpemUud2lkdGggKyBcInB4XCI7XG4gICAgY29udGFpbmVyLnN0eWxlLmhlaWdodCA9IHNpemUuaGVpZ2h0ICsgXCJweFwiO1xuICAgIGNvbnRhaW5lci5zdHlsZS5wb3NpdGlvbiA9IFwiYWJzb2x1dGVcIjtcbiAgICByZXR1cm4gY29udGFpbmVyO1xuICB9XG59IiwiaW1wb3J0IHsgU2l6ZSB9IGZyb20gXCIuL3NpemVcIjtcblxuZXhwb3J0IGNsYXNzIEdyYXBoQ29uZmlndXJhdGlvbiB7XG4gIGNsYXNzTmFtZTogc3RyaW5nO1xuICBzaWRlUGFkZGluZzogbnVtYmVyO1xuICBtYXJnaW5CZXR3ZWVuRHJhd2FibGVzOiBudW1iZXI7XG4gIGxpbmVDb2xvcjogc3RyaW5nO1xuICBjb2x1bW5Db3VudDogbnVtYmVyO1xuICBkcmF3QXJlYTogSFRNTEVsZW1lbnQ7XG4gIHNpemU6IFNpemU7XG5cbiAgY29uc3RydWN0b3IoZHJhd0FyZWE6IEhUTUxFbGVtZW50LFxuICAgIGNvbHVtbkNvdW50OiBudW1iZXIsXG4gICAgb3B0aW9ucz86IHtcbiAgICAgIGNsYXNzTmFtZT86IHN0cmluZyxcbiAgICAgIHNpZGVQYWRkaW5nPzogbnVtYmVyLFxuICAgICAgbWFyZ2luQmV0d2VlbkRyYXdhYmxlcz86IG51bWJlcixcbiAgICAgIGxpbmVDb2xvcj86IHN0cmluZ1xuICAgIH1cbiAgKSB7XG5cbiAgICB0aGlzLmRyYXdBcmVhID0gZHJhd0FyZWE7XG4gICAgdGhpcy5zaXplID0geyB3aWR0aDogZHJhd0FyZWEub2Zmc2V0V2lkdGgsIGhlaWdodDogZHJhd0FyZWEub2Zmc2V0SGVpZ2h0IH07XG4gICAgdGhpcy5jb2x1bW5Db3VudCA9IGNvbHVtbkNvdW50O1xuICAgIGlmICh0eXBlb2Ygb3B0aW9ucyAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIHRoaXMuY2xhc3NOYW1lID0gdHlwZW9mIG9wdGlvbnMuY2xhc3NOYW1lICE9PSAndW5kZWZpbmVkJyA/IG9wdGlvbnMuY2xhc3NOYW1lIDogXCJpbmZvLWNhcmRcIjtcbiAgICAgIHRoaXMuc2lkZVBhZGRpbmcgPSB0eXBlb2Ygb3B0aW9ucy5zaWRlUGFkZGluZyAhPT0gJ3VuZGVmaW5lZCcgPyBvcHRpb25zLnNpZGVQYWRkaW5nIDogNjA7XG4gICAgICB0aGlzLm1hcmdpbkJldHdlZW5EcmF3YWJsZXMgPSB0eXBlb2Ygb3B0aW9ucy5tYXJnaW5CZXR3ZWVuRHJhd2FibGVzICE9PSAndW5kZWZpbmVkJyA/IG9wdGlvbnMubWFyZ2luQmV0d2VlbkRyYXdhYmxlcyA6IDUwO1xuICAgICAgdGhpcy5saW5lQ29sb3IgPSB0eXBlb2Ygb3B0aW9ucy5saW5lQ29sb3IgIT09ICd1bmRlZmluZWQnID8gb3B0aW9ucy5saW5lQ29sb3IgOiBcIndoaXRlXCI7XG4gICAgfVxuICB9XG59IiwiaW1wb3J0IHsgRHJhd2FibGUgfSBmcm9tIFwiLi9kcmF3YWJsZVwiO1xuaW1wb3J0IHsgR3JhcGhDb25maWd1cmF0aW9uIH0gZnJvbSBcIi4vZ3JhcGgtY29uZmlndXJhdGlvblwiO1xuaW1wb3J0IHsgQ29udGV4dCB9IGZyb20gXCIuL2NvbnRleHRcIjtcbmltcG9ydCB7IFBvc2l0aW9uIH0gZnJvbSAnLi9wb3NpdGlvbic7XG5cbmV4cG9ydCBjbGFzcyBHcmFwaCB7XG4gIHByaXZhdGUgdGhpbmdzOiBBcnJheTxEcmF3YWJsZT47XG4gIHByaXZhdGUgY29uZmlndXJhdGlvbjogR3JhcGhDb25maWd1cmF0aW9uO1xuICBwcml2YXRlIGluZm9jYXJkQ29udGFpbmVyOiBIVE1MRWxlbWVudDtcbiAgcHJpdmF0ZSBjYW52YXNFbGVtZW50OiBIVE1MRWxlbWVudDtcbiAgcHJpdmF0ZSBjb2x1bW5Db3VudDogbnVtYmVyO1xuICBwcml2YXRlIGNvbHVtbldpZHRoOiBudW1iZXI7XG4gIHByaXZhdGUgY29udGV4dDogQ29udGV4dDtcblxuICBjb25zdHJ1Y3Rvcihjb25maWc6IEdyYXBoQ29uZmlndXJhdGlvbiwgdGhpbmdzOiBBcnJheTxEcmF3YWJsZT4pIHtcbiAgICB0aGlzLmNvbmZpZ3VyYXRpb24gPSBjb25maWc7XG4gICAgdGhpcy5jb2x1bW5Db3VudCA9IHRoaXMuY29uZmlndXJhdGlvbi5jb2x1bW5Db3VudDtcbiAgICB0aGlzLnRoaW5ncyA9IHRoaW5ncztcbiAgICB0aGlzLmNvbnRleHQgPSBuZXcgQ29udGV4dCh0aGlzLmNvbmZpZ3VyYXRpb24uZHJhd0FyZWEsIHRoaXMuY29uZmlndXJhdGlvbi5zaXplKTtcbiAgfVxuXG4gIHB1YmxpYyBzZXREYXRhKHRoaW5nczogQXJyYXk8RHJhd2FibGU+KTogdm9pZCB7XG4gICAgdGhpcy50aGluZ3MgPSB0aGluZ3M7XG4gIH1cblxuICBwdWJsaWMgZHJhdygpOiB2b2lkIHtcbiAgICBsZXQgY3Vyc29yOiBQb3NpdGlvbiA9IG5ldyBQb3NpdGlvbigwLCB0aGlzLmNvbmZpZ3VyYXRpb24ubWFyZ2luQmV0d2VlbkRyYXdhYmxlcyk7XG4gICAgdGhpcy50aGluZ3MuZm9yRWFjaCgodGhpbmc6IERyYXdhYmxlLCBpbmRleDogbnVtYmVyKSA9PiB7XG4gICAgICB0aGluZy5kcmF3KHRoaXMuY29udGV4dCwgY3Vyc29yKTtcbiAgICAgIGN1cnNvci54ID0gMDtcbiAgICAgIGN1cnNvci55ICs9IHRoaXMuY29uZmlndXJhdGlvbi5tYXJnaW5CZXR3ZWVuRHJhd2FibGVzO1xuICAgIH0pO1xuICAgIHRoaXMucmVzaXplSGVpZ2h0T2ZDb250ZXh0KGN1cnNvci55KTtcbiAgfVxuXG4gIHB1YmxpYyByZWRyYXcoKTogdm9pZCB7XG4gICAgdGhpcy5jbGVhcigpO1xuICAgIHRoaXMuZHJhdygpO1xuICB9XG5cbiAgcHVibGljIHJlZHJhd1dpdGhEYXRhKHRoaW5nczogQXJyYXk8RHJhd2FibGU+KTogdm9pZCB7XG4gICAgdGhpcy5zZXREYXRhKHRoaW5ncyk7XG4gICAgdGhpcy5yZWRyYXcoKTtcbiAgfVxuXG4gIHByaXZhdGUgY2xlYXIoKTogdm9pZCB7XG4gICAgdGhpcy5jb250ZXh0LmNsZWFyKCk7XG4gIH1cblxuICBwcml2YXRlIHJlc2l6ZUhlaWdodE9mQ29udGV4dChoZWlnaHQ6IG51bWJlcik6IHZvaWQge1xuICAgIHRoaXMuY29udGV4dC5pbmZvQ2FyZENvbnRhaW5lci5zdHlsZS5oZWlnaHQgPSBoZWlnaHQgKyBcInB4XCI7XG4gICAgdGhpcy5jb250ZXh0LmNhbnZhc0NvbnRhaW5lci5xdWVyeVNlbGVjdG9yKFwiI2NhbnZhc1wiKS5zZXRBdHRyaWJ1dGUoXCJoZWlnaHRcIiwgaGVpZ2h0ICsgXCJweFwiKTtcbiAgfVxufSIsImV4cG9ydCAqIGZyb20gJy4vY29ubmVjdGlvbi1wb2ludCc7XG5leHBvcnQgKiBmcm9tICcuL2Nvbm5lY3Rpb24nO1xuZXhwb3J0ICogZnJvbSAnLi9jb250ZXh0JztcbmV4cG9ydCAqIGZyb20gJy4vZHJhd2FibGUnO1xuZXhwb3J0ICogZnJvbSAnLi9ncmFwaC1jb25maWd1cmF0aW9uJztcbmV4cG9ydCAqIGZyb20gJy4vZ3JhcGgnO1xuZXhwb3J0ICogZnJvbSAnLi9pbmZvLWNhcmQtY29uZmlnJztcbmV4cG9ydCAqIGZyb20gJy4vaW5mby1jYXJkJztcbmV4cG9ydCAqIGZyb20gJy4vcGFjay1jb25maWd1cmF0aW9uJztcbmV4cG9ydCAqIGZyb20gJy4vcGFjayc7XG5leHBvcnQgKiBmcm9tICcuL3Bvc2l0aW9uJztcbmV4cG9ydCAqIGZyb20gJy4vc2l6ZSc7XG4iLCJleHBvcnQgY2xhc3MgSW5mb2NhcmRDb25maWcge1xuICBjb2xzcGFuOiBudW1iZXI7XG4gIGZpbGx3aWR0aDogYm9vbGVhbjtcbiAgY29ubmVjdGlvblBvaW50Q29sb3JzOiB7XG4gICAgcHJldjogc3RyaW5nLFxuICAgIG5leHQ6IHN0cmluZ1xuICB9XG5cbiAgY29uc3RydWN0b3Iob3B0aW9ucz86IHtcbiAgICBjb25uZWN0aW9uUG9pbnRDb2xvcnM/OiBhbnlcbiAgfSkge1xuICAgIHRoaXMuY29sc3BhbiA9IDE7XG4gICAgdGhpcy5maWxsd2lkdGggPSBmYWxzZTtcbiAgICBpZiAodHlwZW9mIG9wdGlvbnMuY29ubmVjdGlvblBvaW50Q29sb3JzICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgaWYgKHR5cGVvZiBvcHRpb25zLmNvbm5lY3Rpb25Qb2ludENvbG9ycyA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgdGhpcy5jb25uZWN0aW9uUG9pbnRDb2xvcnMgPSB7XG4gICAgICAgICAgcHJldjogb3B0aW9ucy5jb25uZWN0aW9uUG9pbnRDb2xvcnMsXG4gICAgICAgICAgbmV4dDogb3B0aW9ucy5jb25uZWN0aW9uUG9pbnRDb2xvcnNcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5jb25uZWN0aW9uUG9pbnRDb2xvcnMgPSBvcHRpb25zLmNvbm5lY3Rpb25Qb2ludENvbG9ycztcbiAgICAgIH1cbiAgICB9XG4gIH1cbn0iLCJpbXBvcnQgeyBDb25uZWN0aW9uIH0gZnJvbSAnLi9jb25uZWN0aW9uJztcbmltcG9ydCB7IEluZm9jYXJkQ29uZmlnIH0gZnJvbSAnLi9pbmZvLWNhcmQtY29uZmlnJztcbmltcG9ydCB7IFBhY2sgfSBmcm9tICcuL3BhY2snO1xuXG5leHBvcnQgY2xhc3MgSW5mb0NhcmQge1xuICBpZGVudGlmaWVyOiBzdHJpbmc7XG4gIGNvbnRlbnQ6IEhUTUxFbGVtZW50O1xuICBhc3NvY2lhdGVkUGFjazogUGFjaztcbiAgY29ubmVjdGlvbnM6IHtcbiAgICBwcmV2OiBBcnJheTxDb25uZWN0aW9uPjtcbiAgICBuZXh0OiBBcnJheTxDb25uZWN0aW9uPjtcbiAgfVxuICBkcmF3bjogYm9vbGVhbjtcbiAgY29uZmlnOiBJbmZvY2FyZENvbmZpZztcblxuICBjb25zdHJ1Y3RvcihpZGVudGlmaWVyOiBzdHJpbmcsIGNvbnRlbnQ6IEhUTUxFbGVtZW50LCBjb25maWc6IEluZm9jYXJkQ29uZmlnKSB7XG4gICAgdGhpcy5pZGVudGlmaWVyID0gaWRlbnRpZmllcjtcbiAgICB0aGlzLmNvbnRlbnQgPSBjb250ZW50O1xuICAgIHRoaXMuZHJhd24gPSBmYWxzZTtcbiAgICB0aGlzLmNvbm5lY3Rpb25zID0geyBwcmV2OiBbXSwgbmV4dDogW10gfTtcbiAgICB0aGlzLmNvbmZpZyA9IGNvbmZpZztcbiAgfVxuXG4gIHB1YmxpYyBhdHRhY2hPbk1vdXNlSG92ZXJFdmVudExpc3RlbmVyKCk6IHZvaWQge1xuICAgIHRoaXMuY29udGVudC5hZGRFdmVudExpc3RlbmVyKFwibW91c2VvdmVyXCIsIChldjogTW91c2VFdmVudCkgPT4ge1xuICAgICAgdGhpcy5hc3NvY2lhdGVkUGFjay50b2dnbGVEaW0odHJ1ZSk7XG4gICAgICB0aGlzLmhpZ2hsaWdodENvbm5lY3RlZENhcmRzKCk7XG4gICAgfSwgZmFsc2UpO1xuICAgIHRoaXMuY29udGVudC5hZGRFdmVudExpc3RlbmVyKFwibW91c2VsZWF2ZVwiLCAoZXY6IE1vdXNlRXZlbnQpID0+IHtcbiAgICAgIHRoaXMuYXNzb2NpYXRlZFBhY2sudG9nZ2xlRGltKGZhbHNlKTtcbiAgICB9LCBmYWxzZSk7XG4gIH1cblxuICBwcml2YXRlIGhpZ2hsaWdodENvbm5lY3RlZENhcmRzKCk6IHZvaWQge1xuICAgIHRoaXMuY29udGVudC5zdHlsZS5vcGFjaXR5ID0gXCIxXCI7XG4gICAgdGhpcy5oaWdobGlnaENhcmRzTmV4dCh0aGlzLmNvbm5lY3Rpb25zLm5leHQpO1xuICAgIHRoaXMuaGlnaGxpZ2h0Q2FyZHNQcmV2KHRoaXMuY29ubmVjdGlvbnMucHJldik7XG4gIH1cblxuICBwcml2YXRlIGhpZ2hsaWdoQ2FyZHNOZXh0KGNvbm5zOiBBcnJheTxDb25uZWN0aW9uPik6IHZvaWQge1xuICAgIGNvbm5zLmZvckVhY2goKGNvbm46IENvbm5lY3Rpb24sIGluZGV4OiBudW1iZXIpID0+IHtcbiAgICAgIGNvbm4ucGF0aC5zZXRBdHRyaWJ1dGVOUyhudWxsLCBcInN0cm9rZS1vcGFjaXR5XCIsIFwiMVwiKTtcbiAgICAgIGNvbm4udGhpbmcuY29udGVudC5zdHlsZS5vcGFjaXR5ID0gXCIxXCI7XG4gICAgICB0aGlzLmhpZ2hsaWdoQ2FyZHNOZXh0KGNvbm4udGhpbmcuY29ubmVjdGlvbnMubmV4dCk7XG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIGhpZ2hsaWdodENhcmRzUHJldihjb25uczogQXJyYXk8Q29ubmVjdGlvbj4pOiB2b2lkIHtcbiAgICBjb25ucy5mb3JFYWNoKChjb25uOiBDb25uZWN0aW9uLCBpbmRleDogbnVtYmVyKSA9PiB7XG4gICAgICBjb25uLnBhdGguc2V0QXR0cmlidXRlTlMobnVsbCwgXCJzdHJva2Utb3BhY2l0eVwiLCBcIjFcIik7XG4gICAgICBjb25uLnRoaW5nLmNvbnRlbnQuc3R5bGUub3BhY2l0eSA9IFwiMVwiO1xuICAgICAgdGhpcy5oaWdobGlnaHRDYXJkc1ByZXYoY29ubi50aGluZy5jb25uZWN0aW9ucy5wcmV2KTtcbiAgICB9KTtcbiAgfVxufSIsImltcG9ydCB7IEdyYXBoQ29uZmlndXJhdGlvbiB9IGZyb20gXCIuL2dyYXBoLWNvbmZpZ3VyYXRpb25cIjtcblxuZXhwb3J0IGNsYXNzIFBhY2tDb25maWd1cmF0aW9uIHtcbiAgc2lkZVBhZGRpbmc6IG51bWJlcjtcbiAgY29sdW1uQ291bnQ6IG51bWJlcjtcbiAgY29sdW1uV2lkdGg6IG51bWJlcjtcbiAgZHJhd2luZ01vZGU6IHN0cmluZztcbiAgaW5mb2NhcmRDbGFzc05hbWU6IHN0cmluZztcbiAgcm93TWFyZ2luOiBudW1iZXI7XG4gIGxpbmVDb2xvcjogc3RyaW5nO1xuICBjb25uZWN0aW9uUG9pbnRQb3NpdGlvbjogbnVtYmVyO1xuXG4gIGNvbnN0cnVjdG9yKGRyYXdpbmdNb2RlOiBzdHJpbmcsIGdyYXBoQ29uZmlnOiBHcmFwaENvbmZpZ3VyYXRpb24sIG9wdGlvbnM/OiB7XG4gICAgcm93TWFyZ2luPzogbnVtYmVyO1xuICAgIGNvbm5lY3Rpb25Qb2ludFBvc2l0aW9uPzogbnVtYmVyO1xuICB9KSB7XG4gICAgdGhpcy5kcmF3aW5nTW9kZSA9IGRyYXdpbmdNb2RlO1xuICAgIHRoaXMuaW5mb2NhcmRDbGFzc05hbWUgPSBncmFwaENvbmZpZy5jbGFzc05hbWU7XG4gICAgdGhpcy5zaWRlUGFkZGluZyA9IGdyYXBoQ29uZmlnLnNpZGVQYWRkaW5nO1xuICAgIHRoaXMuY29sdW1uQ291bnQgPSBncmFwaENvbmZpZy5jb2x1bW5Db3VudDtcbiAgICB0aGlzLmNvbHVtbldpZHRoID0gTWF0aC5mbG9vcihncmFwaENvbmZpZy5zaXplLndpZHRoIC8gZ3JhcGhDb25maWcuY29sdW1uQ291bnQpO1xuICAgIHRoaXMubGluZUNvbG9yID0gZ3JhcGhDb25maWcubGluZUNvbG9yO1xuICAgIGlmICh0eXBlb2Ygb3B0aW9ucyAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIHRoaXMucm93TWFyZ2luID0gdHlwZW9mIG9wdGlvbnMucm93TWFyZ2luID09PSAndW5kZWZpbmVkJyA/IDMwIDogb3B0aW9ucy5yb3dNYXJnaW47XG4gICAgICB0aGlzLmNvbm5lY3Rpb25Qb2ludFBvc2l0aW9uID0gdHlwZW9mIG9wdGlvbnMuY29ubmVjdGlvblBvaW50UG9zaXRpb24gPT09ICd1bmRlZmluZWQnID8gbnVsbCA6IG9wdGlvbnMuY29ubmVjdGlvblBvaW50UG9zaXRpb247XG4gICAgfVxuICB9XG59IiwiaW1wb3J0IHsgSW5mb0NhcmQgfSBmcm9tIFwiLi9pbmZvLWNhcmRcIjtcbmltcG9ydCB7IFBhY2tDb25maWd1cmF0aW9uIH0gZnJvbSBcIi4vcGFjay1jb25maWd1cmF0aW9uXCI7XG5pbXBvcnQgeyBEcmF3YWJsZSB9IGZyb20gXCIuL2RyYXdhYmxlXCI7XG5pbXBvcnQgeyBDb250ZXh0IH0gZnJvbSBcIi4vY29udGV4dFwiO1xuaW1wb3J0IHsgUG9zaXRpb24gfSBmcm9tICcuL3Bvc2l0aW9uJztcblxuZXhwb3J0IGNsYXNzIFBhY2sgaW1wbGVtZW50cyBEcmF3YWJsZSB7XG4gIHByaXZhdGUgZ3JpZDogQXJyYXk8QXJyYXk8SW5mb0NhcmQ+PjtcbiAgcHJpdmF0ZSBjb25uZWN0aW9uczogTWFwPHN0cmluZywgQXJyYXk8bnVtYmVyW10+PjtcbiAgcHJpdmF0ZSBjb25maWc6IFBhY2tDb25maWd1cmF0aW9uO1xuICBwcml2YXRlIGluZm9jYXJkV2lkdGg6IG51bWJlcjtcbiAgcHJpdmF0ZSBjb250ZXh0OiBDb250ZXh0O1xuXG4gIGNvbnN0cnVjdG9yKGdyaWQ6IEFycmF5PEFycmF5PEluZm9DYXJkPj4sIGNvbm5lY3Rpb25zOiBNYXA8c3RyaW5nLCBBcnJheTxudW1iZXJbXT4+LCBjb25maWc/OiBQYWNrQ29uZmlndXJhdGlvbikge1xuICAgIHRoaXMuZ3JpZCA9IGdyaWQ7XG4gICAgdGhpcy5jb25uZWN0aW9ucyA9IGNvbm5lY3Rpb25zO1xuICAgIHRoaXMuY29uZmlnID0gY29uZmlnO1xuICAgIHRoaXMuaW5mb2NhcmRXaWR0aCA9IHRoaXMuY29uZmlnLmNvbHVtbldpZHRoIC0gKDIgKiB0aGlzLmNvbmZpZy5zaWRlUGFkZGluZyk7XG4gIH1cblxuICBwdWJsaWMgc2V0Q29uZmlnKGNvbmZpZzogUGFja0NvbmZpZ3VyYXRpb24pOiB2b2lkIHtcbiAgICB0aGlzLmNvbmZpZyA9IGNvbmZpZztcbiAgfVxuXG4gIHB1YmxpYyBkcmF3KGNvbnRleHQ6IENvbnRleHQsIGN1cnNvcjogUG9zaXRpb24pOiB2b2lkIHtcbiAgICB0aGlzLnB1dEluZm9jYXJkcyhjb250ZXh0LCBjdXJzb3IsIHRoaXMuY29uZmlnLmRyYXdpbmdNb2RlKTtcbiAgICB0aGlzLmNvbm5lY3RJbmZvY2FyZHMoY29udGV4dCk7XG4gICAgdGhpcy5jb250ZXh0ID0gY29udGV4dDtcbiAgfVxuXG4gIHB1YmxpYyB0b2dnbGVEaW0oc3RhdGU6IGJvb2xlYW4pOiB2b2lkIHtcbiAgICBsZXQgb3BhY2l0eTogbnVtYmVyID0gc3RhdGUgPyAwLjUgOiAxLjA7XG4gICAgbGV0IHN2Z0VsZW1lbnQ6IFNWR0VsZW1lbnQgPSB0aGlzLmNvbnRleHQuY2FudmFzQ29udGFpbmVyLnF1ZXJ5U2VsZWN0b3IoXCIjY2FudmFzXCIpO1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgc3ZnRWxlbWVudC5jaGlsZHJlbi5sZW5ndGg7IGkrKykge1xuICAgICAgc3ZnRWxlbWVudC5jaGlsZHJlbltpXS5zZXRBdHRyaWJ1dGVOUyhudWxsLCBcInN0cm9rZS1vcGFjaXR5XCIsIG9wYWNpdHkgKyBcIlwiKTtcbiAgICB9XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmNvbnRleHQuaW5mb0NhcmRDb250YWluZXIuY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcbiAgICAgICg8SFRNTEVsZW1lbnQ+dGhpcy5jb250ZXh0LmluZm9DYXJkQ29udGFpbmVyLmNoaWxkTm9kZXNbaV0pLnN0eWxlLm9wYWNpdHkgPSBvcGFjaXR5ICsgXCJcIjtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGNvbm5lY3RJbmZvY2FyZHMoY29udGV4dDogQ29udGV4dCk6IHZvaWQge1xuICAgIHRoaXMuZ3JpZC5mb3JFYWNoKChjb2x1bW5zOiBBcnJheTxJbmZvQ2FyZD4sIHJvd0luZGV4OiBudW1iZXIpID0+IHtcbiAgICAgIGNvbHVtbnMuZm9yRWFjaCgoY2VsbDogSW5mb0NhcmQsIGNvbHVtbkluZGV4OiBudW1iZXIpID0+IHtcbiAgICAgICAgaWYgKGNlbGwgPT09IG51bGwpIHJldHVybjtcbiAgICAgICAgbGV0IGNvbm5lY3Rpb246IEFycmF5PEFycmF5PG51bWJlcj4+ID0gdGhpcy5jb25uZWN0aW9ucy5nZXQoY2VsbC5pZGVudGlmaWVyKTtcbiAgICAgICAgaWYgKGNvbm5lY3Rpb24gPT09IG51bGwgfHwgY29ubmVjdGlvbiA9PT0gdW5kZWZpbmVkKSByZXR1cm47XG4gICAgICAgIGNvbm5lY3Rpb24uZm9yRWFjaCgoY29ubjogQXJyYXk8bnVtYmVyPiwgaW5kZXg6IG51bWJlcikgPT4ge1xuICAgICAgICAgIHRoaXMuY29ubmVjdFR3b0luZm9DYXJkcyhjZWxsLCB0aGlzLmdyaWRbY29ublswXV1bY29ublsxXV0sIGNvbnRleHQpO1xuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBwdXRJbmZvY2FyZHMoY29udGV4dDogQ29udGV4dCwgY3Vyc29yOiBQb3NpdGlvbiwgbW9kZTogc3RyaW5nKTogdm9pZCB7XG4gICAgc3dpdGNoIChtb2RlKSB7XG4gICAgICBjYXNlIFwiY29tcGFjdFwiOiB0aGlzLnB1dEluZm9jYXJkc0luQ29tcGFjdChjb250ZXh0LCBjdXJzb3IpOyBicmVhaztcbiAgICAgIGNhc2UgXCJsb29zZVwiOlxuICAgICAgZGVmYXVsdDpcbiAgICAgICAgdGhpcy5wdXRJbmZvY2FyZHNJbkxvb3NlKGNvbnRleHQsIGN1cnNvcik7IGJyZWFrO1xuICAgIH1cblxuICB9XG5cbiAgcHJpdmF0ZSBwdXRJbmZvY2FyZHNJbkNvbXBhY3QoY29udGV4dDogQ29udGV4dCwgY3Vyc29yOiBQb3NpdGlvbik6IHZvaWQge1xuICAgIGxldCBtYXhIZWlnaHQ6IG51bWJlciA9IDAsIHRlbXBDdXJzb3JZOiBudW1iZXIgPSBjdXJzb3IueTtcbiAgICBmb3IgKGxldCBjb2x1bW5JbmRleCA9IDA7IGNvbHVtbkluZGV4IDwgdGhpcy5jb25maWcuY29sdW1uQ291bnQ7IGNvbHVtbkluZGV4KyspIHtcbiAgICAgIGN1cnNvci54ID0gdGhpcy5jb25maWcuc2lkZVBhZGRpbmcgKyAodGhpcy5jb25maWcuY29sdW1uV2lkdGggKiBjb2x1bW5JbmRleCk7XG4gICAgICBjdXJzb3IueSA9IHRlbXBDdXJzb3JZO1xuICAgICAgbGV0IHRvdGFsSGVpZ2h0OiBudW1iZXIgPSAwO1xuICAgICAgdGhpcy5ncmlkLmZvckVhY2goKGNvbHVtbnM6IEFycmF5PEluZm9DYXJkPikgPT4ge1xuICAgICAgICBpZiAoY29sdW1uc1tjb2x1bW5JbmRleF0gPT09IG51bGwpIHJldHVybjtcbiAgICAgICAgbGV0IGhlaWdodDogbnVtYmVyID0gdGhpcy5kcmF3SW5mb2NhcmQoY3Vyc29yLCBjb250ZXh0LCBjb2x1bW5zW2NvbHVtbkluZGV4XSk7XG4gICAgICAgIGNvbHVtbnNbY29sdW1uSW5kZXhdLmFzc29jaWF0ZWRQYWNrID0gdGhpcztcbiAgICAgICAgY29sdW1uc1tjb2x1bW5JbmRleF0uYXR0YWNoT25Nb3VzZUhvdmVyRXZlbnRMaXN0ZW5lcigpO1xuICAgICAgICBjdXJzb3IueSArPSBoZWlnaHQgKyB0aGlzLmNvbmZpZy5yb3dNYXJnaW47XG4gICAgICAgIHRvdGFsSGVpZ2h0ICs9IGhlaWdodCArIHRoaXMuY29uZmlnLnJvd01hcmdpbjtcbiAgICAgIH0pO1xuICAgICAgbWF4SGVpZ2h0ID0gdG90YWxIZWlnaHQgPiBtYXhIZWlnaHQgPyB0b3RhbEhlaWdodCA6IG1heEhlaWdodDtcbiAgICB9XG4gICAgY3Vyc29yLnkgPSBtYXhIZWlnaHQ7XG4gIH1cblxuICBwcml2YXRlIHB1dEluZm9jYXJkc0luTG9vc2UoY29udGV4dDogQ29udGV4dCwgY3Vyc29yOiBQb3NpdGlvbik6IHZvaWQge1xuICAgIHRoaXMuZ3JpZC5mb3JFYWNoKChjb2x1bW5zOiBBcnJheTxJbmZvQ2FyZD4sIHJvd0luZGV4OiBudW1iZXIpID0+IHtcbiAgICAgIGxldCBtYXhIZWlnaHQ6IG51bWJlciA9IDA7XG4gICAgICBjdXJzb3IueCA9IHRoaXMuY29uZmlnLnNpZGVQYWRkaW5nO1xuICAgICAgY29sdW1ucy5mb3JFYWNoKChjYXJkOiBJbmZvQ2FyZCwgY29sdW1uSW5kZXg6IG51bWJlcikgPT4ge1xuICAgICAgICBpZiAoY2FyZCA9PT0gbnVsbCkge1xuICAgICAgICAgIGN1cnNvci54ICs9IHRoaXMuY29uZmlnLmNvbHVtbldpZHRoO1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBsZXQgaGVpZ2h0OiBudW1iZXIgPSB0aGlzLmRyYXdJbmZvY2FyZChjdXJzb3IsIGNvbnRleHQsIGNhcmQpO1xuICAgICAgICBjdXJzb3IueCArPSB0aGlzLmNvbmZpZy5jb2x1bW5XaWR0aDtcbiAgICAgICAgbWF4SGVpZ2h0ID0gbWF4SGVpZ2h0IDwgaGVpZ2h0ID8gaGVpZ2h0IDogbWF4SGVpZ2h0O1xuICAgICAgICBjYXJkLmFzc29jaWF0ZWRQYWNrID0gdGhpcztcbiAgICAgICAgY2FyZC5hdHRhY2hPbk1vdXNlSG92ZXJFdmVudExpc3RlbmVyKCk7XG4gICAgICB9KTtcbiAgICAgIGN1cnNvci55ICs9IG1heEhlaWdodCArIHRoaXMuY29uZmlnLnJvd01hcmdpbjtcbiAgICB9KVxuICB9XG5cbiAgcHJpdmF0ZSBkcmF3SW5mb2NhcmQoY3Vyc29yOiBQb3NpdGlvbiwgY29udGV4dDogQ29udGV4dCwgY2FyZDogSW5mb0NhcmQpOiBudW1iZXIge1xuICAgIGNhcmQuY29udGVudC5zdHlsZS5wb3NpdGlvbiA9IFwiYWJzb2x1dGVcIjtcbiAgICBjYXJkLmNvbnRlbnQuY2xhc3NOYW1lID0gdGhpcy5jb25maWcuaW5mb2NhcmRDbGFzc05hbWUgKyBcIiBcIiArIGNhcmQuY29udGVudC5jbGFzc05hbWU7XG4gICAgY2FyZC5jb250ZW50LnN0eWxlLndpZHRoID0gdGhpcy5pbmZvY2FyZFdpZHRoICsgXCJweFwiO1xuICAgIGNhcmQuY29udGVudC5zdHlsZS50b3AgPSBjdXJzb3IueSArIFwicHhcIjtcbiAgICBjYXJkLmNvbnRlbnQuc3R5bGUubGVmdCA9IGN1cnNvci54ICsgXCJweFwiO1xuICAgIGNhcmQuY29udGVudC5zdHlsZS5ib3hTaXppbmcgPSBcImJvcmRlci1ib3hcIjtcbiAgICBjYXJkLmNvbnRlbnQuc3R5bGUudHJhbnNpdGlvbiA9IFwib3BhY2l0eSAwLjJzIGxpbmVhclwiO1xuICAgIGNvbnRleHQuaW5mb0NhcmRDb250YWluZXIuYXBwZW5kQ2hpbGQoY2FyZC5jb250ZW50KTtcbiAgICBjYXJkLmRyYXduID0gdHJ1ZTtcblxuICAgIC8vY3Vyc29yLnggKz0gdGhpcy5jb25maWcuY29sdW1uV2lkdGg7XG4gICAgcmV0dXJuIGNhcmQuY29udGVudC5vZmZzZXRIZWlnaHQ7XG4gIH1cblxuICBwcml2YXRlIGNvbm5lY3RUd29JbmZvQ2FyZHMoc3RhcnQ6IEluZm9DYXJkLCBlbmQ6IEluZm9DYXJkLCBjb250ZXh0OiBDb250ZXh0KTogdm9pZCB7XG4gICAgbGV0IHN0YXJ0WSA9IHRoaXMuY29uZmlnLmNvbm5lY3Rpb25Qb2ludFBvc2l0aW9uID09PSBudWxsID9cbiAgICAgIE1hdGguZmxvb3Ioc3RhcnQuY29udGVudC5vZmZzZXRIZWlnaHQgLyAyKSA6IHRoaXMuY29uZmlnLmNvbm5lY3Rpb25Qb2ludFBvc2l0aW9uO1xuICAgIGxldCBzUG9zaXRpb246IFBvc2l0aW9uID0gbmV3IFBvc2l0aW9uKFxuICAgICAgcGFyc2VJbnQoc3RhcnQuY29udGVudC5zdHlsZS5sZWZ0LnJlcGxhY2UoXCJweFwiLCBcIlwiKSkgKyBzdGFydC5jb250ZW50Lm9mZnNldFdpZHRoLFxuICAgICAgcGFyc2VJbnQoc3RhcnQuY29udGVudC5zdHlsZS50b3AucmVwbGFjZShcInB4XCIsIFwiXCIpKSArIHN0YXJ0WVxuICAgICk7XG5cbiAgICBsZXQgZW5kWSA9IHRoaXMuY29uZmlnLmNvbm5lY3Rpb25Qb2ludFBvc2l0aW9uID09PSBudWxsID9cbiAgICAgIE1hdGguZmxvb3IoZW5kLmNvbnRlbnQub2Zmc2V0SGVpZ2h0IC8gMikgOiB0aGlzLmNvbmZpZy5jb25uZWN0aW9uUG9pbnRQb3NpdGlvbjtcbiAgICBsZXQgZVBvc2l0aW9uOiBQb3NpdGlvbiA9IG5ldyBQb3NpdGlvbihcbiAgICAgIHBhcnNlSW50KGVuZC5jb250ZW50LnN0eWxlLmxlZnQucmVwbGFjZShcInB4XCIsIFwiXCIpKSxcbiAgICAgIHBhcnNlSW50KGVuZC5jb250ZW50LnN0eWxlLnRvcC5yZXBsYWNlKFwicHhcIiwgXCJcIikpICsgZW5kWVxuICAgICk7XG5cbiAgICBsZXQgc3ZnOiBTVkdFbGVtZW50ID0gY29udGV4dC5jYW52YXNDb250YWluZXIucXVlcnlTZWxlY3RvcihcIiNjYW52YXNcIik7XG4gICAgbGV0IHBhdGg6IFNWR1BhdGhFbGVtZW50ID0gdGhpcy5kcmF3Q3VydmVkTGluZShzUG9zaXRpb24sIGVQb3NpdGlvbiwgc3ZnKTtcbiAgICB0aGlzLnB1dENvbm5lY3Rpb25Qb2ludChzdGFydCwgXCJzdGFydFwiKTtcbiAgICB0aGlzLnB1dENvbm5lY3Rpb25Qb2ludChlbmQsIFwiZW5kXCIpO1xuICAgIHN0YXJ0LmNvbm5lY3Rpb25zLm5leHQucHVzaCh7IHBhdGg6IHBhdGgsIHRoaW5nOiBlbmQgfSk7XG4gICAgZW5kLmNvbm5lY3Rpb25zLnByZXYucHVzaCh7IHBhdGg6IHBhdGgsIHRoaW5nOiBzdGFydCB9KTtcbiAgfVxuXG4gIHByaXZhdGUgZHJhd0N1cnZlZExpbmUoc3RhcnQ6IFBvc2l0aW9uLCBlbmQ6IFBvc2l0aW9uLCBzdmc6IFNWR0VsZW1lbnQsIGRlYnVnTW9kZTogYm9vbGVhbiA9IGZhbHNlKTogU1ZHUGF0aEVsZW1lbnQge1xuICAgIGxldCBwYXRoRWxlbWVudDogU1ZHUGF0aEVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50TlMoXCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiLCBcInBhdGhcIik7XG4gICAgbGV0IHhDZW50ZXI6IG51bWJlciA9IHN0YXJ0LnggKyBNYXRoLmZsb29yKChlbmQueCAtIHN0YXJ0LngpIC8gMik7XG5cbiAgICBsZXQgcGF0aFN0cmluZyA9IFwiTSBcIiArIHN0YXJ0LnggKyBcIixcIiArIHN0YXJ0LnkgKyBcIiBDIFwiICsgeENlbnRlciArIFwiLFwiICsgc3RhcnQueSArIFwiIFwiICsgeENlbnRlciArIFwiLFwiICsgZW5kLnkgK1xuICAgICAgXCIgXCIgKyBlbmQueCArIFwiLFwiICsgZW5kLnk7XG4gICAgcGF0aEVsZW1lbnQuc2V0QXR0cmlidXRlTlMobnVsbCwgXCJkXCIsIHBhdGhTdHJpbmcpO1xuICAgIHBhdGhFbGVtZW50LnNldEF0dHJpYnV0ZU5TKG51bGwsIFwiZmlsbFwiLCBcInRyYW5zcGFyZW50XCIpO1xuICAgIHBhdGhFbGVtZW50LnNldEF0dHJpYnV0ZU5TKG51bGwsIFwic3Ryb2tlXCIsIHRoaXMuY29uZmlnLmxpbmVDb2xvcik7XG4gICAgcGF0aEVsZW1lbnQuc2V0QXR0cmlidXRlTlMobnVsbCwgXCJzdHJva2Utd2lkdGhcIiwgXCIyXCIpO1xuICAgIHBhdGhFbGVtZW50LnN0eWxlLnRyYW5zaXRpb24gPSBcInN0cm9rZS1vcGFjaXR5IDAuMnMgbGluZWFyXCI7XG4gICAgc3ZnLmFwcGVuZENoaWxkKHBhdGhFbGVtZW50KTtcblxuICAgIHJldHVybiBwYXRoRWxlbWVudDtcbiAgfVxuXG4gIHByaXZhdGUgcHV0Q29ubmVjdGlvblBvaW50KGluZm9jYXJkOiBJbmZvQ2FyZCwgd2hlcmU6IHN0cmluZyk6IHZvaWQge1xuICAgIGxldCBwb2ludDogSFRNTERpdkVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO1xuICAgIHBvaW50LnN0eWxlLmJveFNpemluZyA9IFwiYm9yZGVyLWJveFwiO1xuICAgIHBvaW50LnN0eWxlLndpZHRoID0gXCIxMnB4XCI7XG4gICAgcG9pbnQuc3R5bGUuaGVpZ2h0ID0gXCIxMnB4XCI7XG4gICAgcG9pbnQuc3R5bGUucG9zaXRpb24gPSBcImFic29sdXRlXCI7XG4gICAgcG9pbnQuc3R5bGUuYm9yZGVyUmFkaXVzID0gXCI1MCVcIjtcbiAgICBwb2ludC5zdHlsZS5iYWNrZ3JvdW5kQ29sb3IgPSBcImJsYWNrXCI7XG5cbiAgICBsZXQgeVBvczogbnVtYmVyID0gdGhpcy5jb25maWcuY29ubmVjdGlvblBvaW50UG9zaXRpb24gPT09IG51bGwgP1xuICAgICAgTWF0aC5mbG9vcihpbmZvY2FyZC5jb250ZW50Lm9mZnNldEhlaWdodCAvIDIpIDogdGhpcy5jb25maWcuY29ubmVjdGlvblBvaW50UG9zaXRpb247XG4gICAgc3dpdGNoICh3aGVyZSkge1xuICAgICAgY2FzZSBcImVuZFwiOlxuICAgICAgICBwb2ludC5zdHlsZS5sZWZ0ID0gXCItNnB4XCI7XG4gICAgICAgIHBvaW50LnN0eWxlLmJhY2tncm91bmRDb2xvciA9IGluZm9jYXJkLmNvbmZpZy5jb25uZWN0aW9uUG9pbnRDb2xvcnMubmV4dDtcbiAgICAgICAgcG9pbnQuc3R5bGUuYm9yZGVyID0gXCIxcHggc29saWQgXCIgKyBpbmZvY2FyZC5jb25maWcuY29ubmVjdGlvblBvaW50Q29sb3JzLm5leHQ7XG4gICAgICAgIGJyZWFrO1xuICAgICAgZGVmYXVsdDpcbiAgICAgICAgcG9pbnQuc3R5bGUucmlnaHQgPSBcIi02cHhcIjtcbiAgICAgICAgcG9pbnQuc3R5bGUuYmFja2dyb3VuZENvbG9yID0gaW5mb2NhcmQuY29uZmlnLmNvbm5lY3Rpb25Qb2ludENvbG9ycy5wcmV2O1xuICAgICAgICBwb2ludC5zdHlsZS5ib3JkZXIgPSBcIjFweCBzb2xpZCBcIiArIGluZm9jYXJkLmNvbmZpZy5jb25uZWN0aW9uUG9pbnRDb2xvcnMucHJldjtcbiAgICAgICAgYnJlYWs7XG4gICAgfVxuICAgIHBvaW50LnN0eWxlLnRvcCA9ICh5UG9zIC0gNikgKyBcInB4XCI7XG4gICAgaW5mb2NhcmQuY29udGVudC5hcHBlbmRDaGlsZChwb2ludCk7XG4gIH1cbn0iLCJleHBvcnQgY2xhc3MgUG9zaXRpb24ge1xuICB4OiBudW1iZXI7XG4gIHk6IG51bWJlcjtcblxuICBwdWJsaWMgY2xvbmUoKTogUG9zaXRpb24ge1xuICAgIHJldHVybiBuZXcgUG9zaXRpb24odGhpcy54LCB0aGlzLnkpO1xuICB9XG5cbiAgY29uc3RydWN0b3IoeDogbnVtYmVyLCB5OiBudW1iZXIpIHtcbiAgICB0aGlzLnggPSB4O1xuICAgIHRoaXMueSA9IHk7XG4gIH1cbn0iLCJleHBvcnQgY2xhc3MgU2l6ZSB7XG4gIHdpZHRoOiBudW1iZXI7XG4gIGhlaWdodDogbnVtYmVyO1xufSJdLCJzb3VyY2VSb290IjoiIn0=