var randomHeight = function (lower, upper) {
  return Math.floor(lower + (upper - lower) * Math.random());
}

var createElement = function (content) {
  var element = document.createElement("div");
  element.appendChild(document.createTextNode(content));
  return element;
}

var createInfocard = function (content, pack) {
  var element = createElement(content);
  var config = new SankeyGraph.InfocardConfig(/*{ connectionPointColors: '#01f1ff' }*/);
  var infocard = new SankeyGraph.InfoCard("ic-" + content, element, config);
  return infocard;
}

var config = new SankeyGraph.GraphConfiguration(document.getElementById("draw-area"), 3, { lineColor: '#01f1ff', sidePadding: 30 });
var graph = new SankeyGraph.Graph(config, null);


var grid = [
  [],
  [],
  []
];
[1, 2, 3].forEach(function (e) { grid[0].push(createInfocard(e)) });
[null, 4, null].forEach(function (e) { if (e === null) { grid[1].push(null); return; } grid[1].push(createInfocard(e)); });
[5, null, 6].forEach(function (e) { if (e === null) { grid[2].push(null); return; } grid[2].push(createInfocard(e)); });

var connections = new Map();
grid.forEach(function (row, rIndex) {
  row.forEach(function (cell, cIndex) {
    switch (rIndex) {
      case 0:
        switch (cIndex) {
          case 0: connections.set("ic-1", []); connections.get("ic-1").push([0, 1]); break;
          case 1: connections.set("ic-2", []); connections.get("ic-2").push([0, 2]); break;
          case 2: connections.set("ic-3", null); break;
        }
        break;

      case 1:
        switch (cIndex) {
          case 1: connections.set("ic-4", []); connections.get("ic-4").push([0, 2]); break;
        }
        break;

      case 2:
        switch (cIndex) {
          case 0: connections.set("ic-5", []); connections.get("ic-5").push([2, 2]); break;
          case 2: connections.set("ic-6", null); break;
        }
        break;
    }
  });
});

var pack = new SankeyGraph.Pack(grid, connections, new SankeyGraph.PackConfiguration(
  'loose',
  config, { connectionPointPosition: 50 }
));

graph.setData([pack]);
graph.draw();

window.addEventListener('resize', (event) => {
  console.log(document.getElementById('draw-area').offsetWidth, graph);
  graph.redraw();
});