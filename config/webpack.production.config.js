const path = require('path');

module.exports = {
  entry: path.resolve(__dirname, '..', 'src', 'index.ts'),
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.ts$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
        options: {
          configFile: path.resolve(__dirname, 'tsconfig.production.json')
        }
      }
    ]
  },
  resolve: {
    extensions: [ '.ts', '.js' ]
  },
  output: {
    filename: 'bundle.min.js',
    path: path.resolve(__dirname, '..', 'dist'),
    library: 'SankeyGraph'
  }
};