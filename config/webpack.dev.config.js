const path = require('path');

module.exports = {
  entry: path.resolve(__dirname, '..', 'src', 'index.ts'),
  mode: 'development',
  devtool: 'inline-source-map',
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        loader: 'ts-loader',
        options: {
          configFile: path.resolve(__dirname, 'tsconfig.dev.json')
        }
      }
    ]
  },
  resolve: {
    extensions: [ '.ts', '.js' ]
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, '..', 'dist'),
    library: 'SankeyGraph'
  },
  watch: true,
  devServer: {
    contentBase: path.resolve(__dirname, '..', 'dist'),
    port: 9090
  }
};